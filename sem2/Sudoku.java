import java.io.File; 
import java.util.Scanner; 
import java.util.Random; 

public class Sudoku { 

	public static void main(String[] args) { 

		int [][] mas = new int[9][9]; 
		int count = 0; 
		int[][] mass={{2, 3, 8, 4, 5, 1, 6, 9, 7}, {1, 5, 6, 7, 2, 9, 3, 4, 8}, {9, 4, 7, 6, 8, 3, 1, 5, 2}, 
		{8, 7, 1, 2, 3, 5, 4, 6, 9}, {4, 2, 5, 1, 9, 6, 7, 8, 3}, {3, 6, 9, 8, 4, 7, 5, 2, 1}, 
		{7, 8, 2, 3, 6, 4, 9, 1, 5}, {6, 9, 3, 5, 1, 2, 8, 7, 4}, {5, 1, 4, 9, 7, 8, 2, 3, 6}}; 

		Random R_num=new Random(); 
		int row1,row2,col1,col2; 
		int change[]=new int[9]; 
		int g = 10+R_num.nextInt(10); 
		for(int y=0;y<g;y++){ 
			for(int a=0;a<3;a++){ 
				row1=0; 
				row2=0; 
				if(a==0){ 
					row1=R_num.nextInt(3); 
					row2=R_num.nextInt(3); 
				} 
				else if(a==1){ 
					row1=3+R_num.nextInt(3); 
					row2=3+R_num.nextInt(3); 
				} 
				else if(a==2){ 
					row1=6+R_num.nextInt(3); 
					row2=6+R_num.nextInt(3); 
				} 
				for(int i=0;i<9;i++){ 
					change[i]=mass[row1][i]; 
					mass[row1][i]=mass[row2][i]; 
					mass[row2][i]=change[i]; 
				} 
				} 
			for(int a=0;a<3;a++){ 
				col1=0; 
				col2=0; 
				if(a==0){ 
					col1=R_num.nextInt(3); 
					col2=R_num.nextInt(3); 
				} 
				else if(a==1){ 
					col1=3+R_num.nextInt(3); 
					col2=3+R_num.nextInt(3); 
				} 
				else if(a==2){ 
					col1=6+R_num.nextInt(3); 
					col2=6+R_num.nextInt(3); 
				} 
				for(int i=0;i<9;i++){ 
					change[i]=mass[i][col1]; 
					mass[i][col1]=mass[i][col2]; 
					mass[i][col2]=change[i]; 
				} 
			} 
		}	  
		
		Scanner s = new Scanner(System.in);
		System.out.println(); 
		System.out.println ("Welcome to *SUDOKU* !! Before you start, please read the rules of the game."); 
		System.out.println(); 
		System.out.println ("RULES: Your task is to fill empty cells (0) with numbers from 1 to 9. In each line and in each column all the digits appear exactly 1 time. To fill a cell, enter column and line coordinates, then enter the number. Good luck!"); 
		System.out.println();
		System.out.println("Choose level: 1 - easy, 2 - medium, 3 - hard "); 
		
		int level = s.nextInt(); 
		int colvo=0; 
		if (level==1){ 
			colvo = 9; 
		} 
		else if (level==2){ 
			colvo = 17; 
		} 
		else if (level==3){ 
			colvo = 50; 
		} 
		
		int row,column; 

		for(int i=0;i<colvo;i++){ 
			row=R_num.nextInt(9); 
			column=R_num.nextInt(9); 
			mass[row][column]=0; 
		} 
		for(int i = 0; i<mass.length; i++){ 
			for (int j = 0; j<mass.length; j++){ 
				mas[i][j] = mass[i][j]; 
				if (mass[i][j]==0){ 
					count += 1; 
				} 
			} 
		} 


		while (count != 0){ 
			System.out.println("    1.2.3.  4.5.6.  7.8.9."); 
			int test = 0; 
			for(int i = 0; i<mas.length; i++){ 
				System.out.println(" "); 
				System.out.print(" " + (i+1) + ". "); 
				for (int j = 0; j<mas.length; j++){ 
					if (mass[i][j]==0){ 
						System.out.print(mas[i][j] + " "); 
					} 
					else{ 
						System.out.print((char) 27 + "[31m" + mas[i][j] + " "); 
					} 
					System.out.print((char) 27 + "[37m"); 
					
					if ((j==2)|(j==5)){ 
						System.out.print("| "); 
					} 
				} 
				if ((i==2)|(i==5)){ 
					System.out.println(); 
					System.out.print("     —----|-------|------"); 
				} 
			} 
			
			System.out.println(" "); 
			System.out.println(" "); 

			System.out.println("Enter column: "); 
			int x = s.nextInt(); 
			while ((x > 9) | (x < 1)){
				System.out.println(" "); 
				System.out.println ("ERROR! Please, enter number of column from 1 to 9:"); 
				x = s.nextInt();
			}
			
			System.out.println("Enter line: "); 
			int y = s.nextInt();
			while ((y> 9) | (y < 1)){
				System.out.println(" "); 
				System.out.println ("ERROR! Please, enter number of line from 1 to 9:"); 
				y = s.nextInt();
			}

			 
			System.out.println("Enter number from 1 to 9: "); 
			int num = s.nextInt(); 
			while (num > 9) {
				System.out.println(" "); 
				System.out.println ("ERROR! Please, enter number from 1 to 9:"); 
				num = s.nextInt();
			}

			 
			 
			for (int j = 0; j<mas.length; j++){ 
				if (mas[j][x-1] == num){ 
				test = 1; 
				} 
				if (mas[y-1][j] == num){ 
				test = 1; 
				} 
			} 
			if ((test == 0)&(mass[y-1][x-1]==0)){ 
				mas[y-1][x-1] = num; 
			} 
			else{ 
				System.out.println(" "); 
				System.out.println ("ERROR! PLease, enter another number."); 
				System.out.println(" "); 
			} 
			count = 0; 
			for(int i = 0; i < mas.length; i++){ 
				for (int j=0;j < mas.length; j++){ 
					if (mas[i][j] == 0) { 
						count +=1; 
					} 
				} 
			} 
		} 
		System.out.println(" 1.2.3. 4.5.6. 7.8.9."); 
		for(int i = 0; i<mas.length; i++){ 
			System.out.println(" "); 
			System.out.print(" " + (i+1) + ". "); 
			for (int j = 0; j<mas.length; j++){ 
				System.out.print((char) 27 + "[31m" + mas[i][j] + " "); 
				System.out.print((char) 27 + "[37m"); 

				if ((j==2)|(j==5)){ 
				System.out.print("| "); 
				} 
			} 
			if ((i==2)|(i==5)){ 
				System.out.println(); 
				System.out.print("     —----|-------|------"); 
			} 
		}

		System.out.println(" ");
		System.out.println(" ");
		System.out.println(" "); 
		System.out.println("Well done!");
		System.out.println(" ");
	} 
}