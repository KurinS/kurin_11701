package sample;

public class Questions {
	private String questions [] = {
		            "Столица Чехии?",
		            "Закончите высказывание:'Волка ...'?",
		            "Сколько лап у паука?",
		            "Что такое ЭВМ?"
	};

	private String choices [][] =  {
					{"Москва", "Прага", "Париж"},
		            {"Ноги кормят", "Ноги носят", "Ноги ломят"},
		            {"6", "4", "8"},
		            {"Электронно-вычислительная машина", "Электронно-вычислительная мама", "Электронно-вычислительная мартышка"}
	};

	private String correctAnswers[] = {"Прага", "Ноги кормят", "8", "Электронно-вычислительная машина"};

	public String getQuestion(int a) {
		return questions[a];
	}


	public String getChoice1(int a) {
		return choices[a][0];
	}


	public String getChoice2(int a) {
		return choices[a][1];
	}

	public String getChoice3(int a) {
		return choices[a][2];
	}

	public String getCorrectAnswer(int a) {
		return correctAnswers[a];
	}
}
