import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;

public class PolinomTest {

    @Test
    public void deleteTest() throws FileNotFoundException {
        Polinom p1 = new Polinom("sampletext");
        p1.delete(2);
        Assert.assertEquals(27, p1.value(3));


    }

    @Test
    public void insertTest() throws FileNotFoundException {
        Polinom p1 = new Polinom("test");
        p1.insert(5, 2);
        Assert.assertEquals("5x^2", (p1.getElements().toString()));

    }

    @Test
    public void derivateTest() throws FileNotFoundException {
        Polinom p1 = new Polinom("sampletext");
        p1.derivate();
        Assert.assertEquals("(16x^(7))+(12x^(5))+(16x^(1))+(3x^(0))", p1.toString());
    }

    @Test
    public void sumTest() throws Exception {
        Polinom p1 = new Polinom("sampletext");
        Polinom p2 = new Polinom("sampletext");
        p1.sum(p2);
        Assert.assertEquals("(4x^(8))+(4x^(6))+(16x^(2))+(6x^(1))", p1.toString());
    }
    @Test
    public void valueTest() throws FileNotFoundException {
        Polinom p1 = new Polinom("sampletext");
        Assert.assertEquals(43,  p1.value(2));

    }

    @Test
    public  void deleteOddTest() throws FileNotFoundException {
        Polinom p1 = new Polinom("sampletext");
        p1.deleteOdd();
        p1.getElements();
        Assert.assertEquals("2x^(8), 2x^(6), 8x^(2)",p1.toString());
    }
}
