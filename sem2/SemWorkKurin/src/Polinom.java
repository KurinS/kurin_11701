import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Polinom {

    private List<Element> elements;

    Polinom(String filename) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(filename));
        elements = new ArrayList<>();
        while (sc.hasNext()) {
            int coef = sc.nextInt();
            int degree = sc.nextInt();
            if (coef != 0) {
                elements.add(new Element(degree, coef));
            }

        }
        sort();
        combine();
    }

    public List<Element> getElements() {
        return elements;
    }

    private void sort() {
        elements.sort(new Comparator<Element>() {
            @Override
            public int compare(Element o1, Element o2) {
                return o2.getDegree() - o1.getDegree();
            }
        });
    }

    @Override
    public String toString() {
        String result = "";
        for (Element x :
                elements) {
            result += "+(" + x.toString() + ")";
        }
        result = result.substring(1, result.length());
        return result;
    }

    public void insert(int coef, int deg) {
        if (coef != 0) {
            elements.add(new Element(deg, coef));
            sort();
            combine();
        }
    }

    private void combine() {
        for (int i = 0; i < elements.size(); i++) {
            Element x = elements.get(i);
            for (int j = i + 1; j < elements.size(); j++) {
                if (x.getDegree() > elements.get(j).getDegree()) {
                    break;
                }
                if (x.getDegree() == elements.get(j).getDegree()) {
                    x.setCoef(x.getCoef() + elements.get(j).getCoef());
                    elements.remove(j);

                }

            }
        }
    }

    public void delete(int deg) {
        for (Element x :
                elements) {
            if (x.getDegree() == deg) {
                elements.remove(x);
                return;
            }
        }
    }

    public void sum(Polinom p) throws Exception {
        if(p ==this){
            throw new Exception("can't sum with itself");
        }
        for (Element x :
                p.getElements()) {
            this.insert(x.getCoef(), x.getDegree());
        }
    }


    public void derivate() {
        for (Element x :
                elements) {
            x.setCoef(x.getCoef() * x.getDegree());
            x.setDegree(x.getDegree() - 1);
        }
    }

    public int value(int x){
        int result = elements.get(0).getCoef();
        for (int i = 1; i < elements.size() ; i++) {
            result = elements.get(i).getCoef() + result * x;
        }
        return result;
    }

    public void deleteOdd(){
        for (int i = 0; i < elements.size(); i++) {
            if(elements.get(i).getDegree() % 2 != 0){
                elements.remove(i);
            }
        }
    }



}
