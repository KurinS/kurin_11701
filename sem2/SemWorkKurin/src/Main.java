import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) throws Exception {
        Polinom p1 = new Polinom("sampletext");
        System.out.println(p1);
        p1.insert(10,2);
        System.out.println("insert 10,2: " + p1);
        System.out.println("Value at 1: " + p1.value(1));
        p1.derivate();
        System.out.println("derivated: " + p1);
        p1.insert(10,2);
        System.out.println("insert 10,2: "+ p1);
        Polinom p2= new Polinom("sampletext");
        p1.sum(p2);
        System.out.println("sum: " + p1);
        p1.deleteOdd();
        System.out.println("deleteOdd: " + p1);
        p1.delete(2);
        System.out.println("Delete degree 2: " + p1);



    }
}
