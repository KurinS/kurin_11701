public class Element {
    private int degree;
    private int coef;

    public Element(int degree, int coef) {
        this.degree = degree;
        this.coef = coef;
    }

    public int getDegree() {
        return degree;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

    public int getCoef() {
        return coef;
    }

    public void setCoef(int coef) {
        this.coef = coef;
    }

    @Override
    public String toString() {
        return coef + "x^" +"(" + degree +")";
    }
}
