import java.util.ArrayList;

public class HeapSort {

    private static int heapSize;


    public static void sort(int[] a) {
        buildHeap(a);
        int n = 0;
        while (heapSize > 1) {
            swap(a, 0, heapSize - 1);
            heapSize--;
            heapify(a, 0);

        }

    }

    public static void sortList(ArrayList<Integer> a) {
        buildHeapList(a);
        int n = 0;
        while (heapSize > 1) {
            swapList(a, 0, heapSize - 1);
            heapSize--;
            heapifyList(a, 0);

        }

    }
    private static void buildHeapList(ArrayList<Integer> a) {
        heapSize = a.size();
        for (int i = a.size() / 2; i >= 0; i--) {
            heapifyList(a, i);
        }
    }

    private static void heapifyList(ArrayList<Integer> a, int i) {
        int l = left(i);
        int r = right(i);
        int largest = i;
        if (l < heapSize && a.get(i) < a.get(l)) {
            largest = l;
        }
        if (r < heapSize && a.get(largest) < a.get(r)) {
            largest = r;
        }
        if (i != largest) {
            swapList(a, i, largest);
            heapifyList(a, largest);
        }
    }

    private static void swapList(ArrayList<Integer> a, int i, int j) {
        int temp = a.get(i);
        a.set(i, a.get(j));
        a.set(j, temp);
    }


    private static void buildHeap(int[] a) {
        heapSize = a.length;
        for (int i = a.length / 2; i >= 0; i--) {
            heapify(a, i);
        }
    }



    private static void heapify(int[] a, int i) {
        int l = left(i);
        int r = right(i);
        int largest = i;
        if (l < heapSize && a[i] < a[l]) {
            largest = l;
        }
        if (r < heapSize && a[largest] < a[r]) {
            largest = r;
        }
        if (i != largest) {
            swap(a, i, largest);
            heapify(a, largest);
        }
    }

    private static int right(int i) {
        return 2 * i + 1;
    }


    private static int left(int i) {
        return 2 * i + 2;
    }


    private static void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

}