import jdk.jfr.events.FileReadEvent;

import java.io.*;
import java.util.*;

public class Main {
    static Random rnd = new Random();
    final static int num = rnd.nextInt(51) + 50;


    public static void main(String[] args) throws FileNotFoundException {
        putIn();
        HeapSort hs = new HeapSort();
        Scanner sc = new Scanner(new File("entry.txt"));
        PrintWriter printWriter = new PrintWriter("file.txt");
        while (sc.hasNext()) {
            int [] array = Arrays.stream(sc.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
            ArrayList<Integer> arrayList = new ArrayList<>();
            for (int i = 0; i < array.length; i++) {
                arrayList.add(array[i]);
            }
            long k1 = System.nanoTime();
            hs.sortList(arrayList);
            long k2 = System.nanoTime();
            long kk1 = System.nanoTime();
            hs.sort(array);
            long kk2 = System.nanoTime();
            printWriter.println(array.length + " " + (k2 - k1) + " " + (kk2 - kk1));
        }
        printWriter.close();

    }


    private static void putIn() {

        try (FileWriter fwrite = new FileWriter("entry.txt", false);) {
            for (int i = 0; i < num; i++) {
                int numch = rnd.nextInt(9901) + 100;
                for (int j = 0; j < numch; j++) {
                    int randnum = rnd.nextInt(5000) * 2;
                    String strrandnum = Integer.toString(randnum);
                    fwrite.write(strrandnum + " ");
                }
                fwrite.append('\n');

            }
            fwrite.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
