package chat.server;

import network.TCPConnection;
import network.TCPConnectionListener;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

public class ChatServer implements TCPConnectionListener {
    private final ArrayList<TCPConnection> connections = new ArrayList<>();

    private ChatServer(){
       System.out.println("Server running");
       try(ServerSocket serverSocket = new ServerSocket(8187)){
           while(true){
               try{
                 new TCPConnection(this, serverSocket.accept());
               } catch (IOException e){
                   System.out.println("Connection exception" + e);
               }
           }
       } catch (IOException e){
           throw new RuntimeException(e);
       }
    }

    public static void main(String[] args) {
        new ChatServer();

    }

    @Override
    public synchronized void onConnectionReady(TCPConnection topConnection) {
        connections.add(topConnection);
        sendToAllConnections("New client connected");
    }

    @Override
    public synchronized void onReceiveString(TCPConnection topConnection, String value) {
       sendToAllConnections(value);
    }

    @Override
    public synchronized void onDisconnect(TCPConnection topConnection) {
        connections.remove(topConnection);
        sendToAllConnections("Client disconnected");
    }

    @Override
    public synchronized void onException(TCPConnection topConnection, Exception e) {
        System.out.println("TCPConnection exception: " + e);
    }

    private void sendToAllConnections(String value){
        System.out.println(value);
        for (int i = 0; i < connections.size(); i++) {
            connections.get(i).sendString(value);
        }
    }

}
