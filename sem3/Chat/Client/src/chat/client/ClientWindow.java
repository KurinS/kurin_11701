package chat.client;

import network.TCPConnection;
import network.TCPConnectionListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class ClientWindow extends JFrame implements ActionListener, TCPConnectionListener {
    private static final String LOCALHOST = "192.168.56.1";
    private static final int PORT = 8187;
    private static final int WIDTH = 600;
    private static final int HEIGHT = 400;
    private TCPConnection connection;




    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ClientWindow();
            }
        });
    }

    private final JTextArea log = new JTextArea();
    private final JTextField nickname = new JTextField("anon");
    private final JTextField fieldInput = new JTextField();


    private ClientWindow(){
      setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
      setSize(WIDTH, HEIGHT);
      setLocationRelativeTo(null);
      setAlwaysOnTop(true);
      log.setEditable(false);
      log.setLineWrap(true);
      add(log, BorderLayout.CENTER);
      fieldInput.addActionListener(this);
      add(fieldInput, BorderLayout.SOUTH);
      add(nickname, BorderLayout.NORTH);
      setVisible(true);
        try {
            connection = new TCPConnection(this, LOCALHOST, PORT);
        } catch (IOException e) {
            printMessage("Connection exception:" + e);
        }

    }

    private synchronized void printMessage(String msg){
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                log.append(msg + "\n");
                log.setCaretPosition(log.getDocument().getLength());
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      String message = fieldInput.getText();
      if (message.equals("")){
          return;
      }
      fieldInput.setText(null);
      connection.sendString(nickname.getText() + ": " + message);
    }


    @Override
    public void onConnectionReady(TCPConnection topConnection) {
        System.out.println("Connection ready");
    }

    @Override
    public void onReceiveString(TCPConnection topConnection, String value) {
        printMessage(value);
    }

    @Override
    public void onDisconnect(TCPConnection topConnection) {
        System.out.println("Connection close");
    }

    @Override
    public void onException(TCPConnection topConnection, Exception e) {
        printMessage("Connection exception");
    }
}
