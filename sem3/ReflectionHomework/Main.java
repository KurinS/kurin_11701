import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        try {
            Class aclass = Class.forName("A");
            Class bclass = Class.forName("B");
            Class cclass = Class.forName("C");
            List <Object> list = new ArrayList();
            Method [] methods;
            list.add(aclass.newInstance());
            list.add(bclass.newInstance());
            list.add(cclass.newInstance());
            for (Object object: list) {
                methods = object.getClass().getDeclaredMethods();
                for(Method method: methods){
                    //System.out.println(method);
                    if (method.getName().startsWith("set")){
                        //System.out.println(method.getName());
                        for (Object object1 : list) {
                            //System.out.println(object1);
                            if (object1.getClass().getName().equals(method.getParameterTypes()[0].getName())){
                                System.out.println(true);
                                method.invoke(object, object1);
                            }
                        }
                    }
                }

            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }


    }
}
