package dao;

import dao.interfaces.PostDAO;
import dao.interfaces.UserDAO;
import enities.Post;
import enities.User;
import services.UserService;

import javax.servlet.http.HttpServletRequest;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PostDAOB implements PostDAO {

    Connection con = SingletonConnection.getConnection();
    private static UserDAO userDAO = new UserDAOB();

    @Override
    public Post getByTitle(String title) {
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from post where title = ?");
            preparedStatement.setString(1, title);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            return new Post(rs.getInt("id"),
                    userDAO.getById(rs.getInt("author_id")),
                    rs.getString("category"),
                    rs.getString("text"),
                    rs.getString("title")
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }



    @Override
    public List<Post> getPostsByCategory(String category) {
        List<Post> posts = new ArrayList<>();
        try {
            PreparedStatement st = con.prepareStatement("select * from post where category =?");
            st.setString(1, category);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                posts.add(new Post(rs.getInt("id"),
                        userDAO.getById(rs.getInt("author_id")),
                        rs.getString("category"),
                        rs.getString("text"),
                        rs.getString("title")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(Arrays.toString(posts.toArray()));
        return posts;
    }

    @Override
    public Post getPostById(int id) {
        try {
            PreparedStatement preparedStatement = con.prepareStatement("select * from post where id = ?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            return new Post(rs.getInt("id"),
                    userDAO.getById(rs.getInt("author_id")),
                    rs.getString("category"),
                    rs.getString("text"),
                    rs.getString("title")
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public void deletePost(Post post) {
        try {
            PreparedStatement preparedStatement = con.prepareStatement("delete from post where id=?");
            preparedStatement.setInt(1, post.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Post> getEvenPosts() {
        List<Post> posts = new ArrayList<>();
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select * from post where  id % 2 = 0 order by id desc ");
            while (rs.next()) {
                posts.add(new Post(rs.getInt("id"),
                        userDAO.getById(rs.getInt("author_id")),
                        rs.getString("category"),
                        rs.getString("text"),
                        rs.getString("title")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return posts;
    }

    @Override
    public List<Post> getUnevenPosts() {
        List<Post> posts = new ArrayList<>();
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select * from post where  id % 2 <> 0 order by id desc");
            while (rs.next()) {
                posts.add(new Post(rs.getInt("id"),
                        userDAO.getById(rs.getInt("author_id")),
                        rs.getString("category"),
                        rs.getString("text"),
                        rs.getString("title")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return posts;
    }

    @Override
    public List<Post> getPosts() {
        List<Post> posts = new ArrayList<>();
        try {
            PreparedStatement st = con.prepareStatement("select * from post");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                posts.add(new Post(rs.getInt("id"),
                        userDAO.getById(rs.getInt("author_id")),
                        rs.getString("category"),
                        rs.getString("text"),
                        rs.getString("title")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return posts;
    }


    @Override
    public int save(HttpServletRequest request) {
        User user;
        try {
            user = userDAO.getByUsername((new UserService()).getCurrentUser(request).getUsername());
            String text = request.getParameter("text");
            String category = request.getParameter("category");
            String title = request.getParameter("title");
            PreparedStatement preparedStatement = con.prepareStatement(
                    "insert  into  post (author_id, category, text, title) values (?, ?, ?, ?)");
            preparedStatement.setInt(1, user.getId());
            preparedStatement.setString(2, category);
            preparedStatement.setString(3, text);
            preparedStatement.setString(4, title);
            preparedStatement.execute();
            return getLastPost().getId();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public Post getLastPost() {
        List<Post> posts = getPosts();
        posts.sort((o1, o2) -> {
            if (o1.getId() > o2.getId()) {
                return 1;
            }
            return -1;
        });
        return posts.get(posts.size() - 1);
    }

    @Override
    public void update(Post post, String text) {
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select * from post where id=" + post.getId());
            rs.next();
            st.execute("update post set text='" + text + "' where id=" + post.getId());
            rs = st.executeQuery("select * from post where id=" + post.getId());
            rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Post> getPostsByAuthorId(int author_id) {
        List<Post> posts = new ArrayList<>();
        try {
            PreparedStatement st = con.prepareStatement("select * from post where author_id = ?");
            st.setInt(1, author_id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                posts.add(new Post(rs.getInt("id"),
                        userDAO.getById(rs.getInt("author_id")),
                        rs.getString("category"),
                        rs.getString("text"),
                        rs.getString("title")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(Arrays.toString(posts.toArray()));
        return posts;
    }
}
