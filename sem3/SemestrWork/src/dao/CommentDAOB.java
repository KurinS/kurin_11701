package dao;

import dao.interfaces.CommentDAO;
import dao.interfaces.PostDAO;
import dao.interfaces.UserDAO;
import enities.Comment;
import enities.Post;
import enities.User;
import services.UserService;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommentDAOB implements CommentDAO {
    Connection con = SingletonConnection.getConnection();
    private static UserDAO userDAO = new UserDAOB();
    private static PostDAO postDao = new PostDAOB();




    @Override
    public void save(HttpServletRequest request) {
        User user;
        Post post;
        try {
            user = userDAO.getByUsername((new UserService()).getCurrentUser(request).getUsername());
            post = postDao.getPostById(Integer.parseInt(request.getPathInfo().substring(1)));
            String text = request.getParameter("text");
            PreparedStatement preparedStatement = con.prepareStatement(
                    "insert  into  commentariy (author_id, post_id, text) values (?, ?, ?)");
            preparedStatement.setInt(1, user.getId());
            preparedStatement.setInt(2, post.getId());
            preparedStatement.setString(3, text);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteComment(HttpServletRequest request) {
        try {
            PreparedStatement preparedStatement = con.prepareStatement("delete from commentariy where id=?");
            preparedStatement.setInt(1,Integer.parseInt(request.getParameter("delete")));
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Comment> getByPostId(int id) {
        List<Comment> comments = new ArrayList<>();
        try {
            PreparedStatement st = con.prepareStatement("select * from commentariy where post_id=?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                comments.add(new Comment(rs.getInt("id"),
                    postDao.getPostById(rs.getInt("post_id")),
                    userDAO.getById(rs.getInt("author_id")),
                    rs.getString("text")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return comments;
    }

    @Override
    public Comment getById(int id) {
        return null;
    }

    @Override
    public Comment getLastComment() {
        List<Comment> comments = getComments();
        comments.sort((o1, o2) -> {
            if (o1.getId() > o2.getId()) {
                return 1;
            }
            return -1;
        });
        return comments.get(comments.size() - 1);
    }

    @Override
    public List<Comment> getComments() {
        return null;
    }
}
