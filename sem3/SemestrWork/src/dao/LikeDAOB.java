package dao;

import dao.interfaces.LikeDAO;
import dao.interfaces.PostDAO;
import dao.interfaces.UserDAO;
import enities.Like;
import enities.Post;
import enities.User;
import services.UserService;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class LikeDAOB implements LikeDAO {
    Connection con = SingletonConnection.getConnection();
    private static UserDAO userDAO = new UserDAOB();
    private static PostDAO postDao = new PostDAOB();

    @Override
    public void deleteLike(int id) {

    }

    @Override
    public void addLike(HttpServletRequest request) {
        User user;
        Post post;

        try {
            user = new UserDAOB().getByUsername(new UserService().getCurrentUser(request).getUsername());
            post = new PostDAOB().getPostById(Integer.parseInt(request.getPathInfo().substring(1)));
            PreparedStatement preparedStatement = con.prepareStatement(
                    "insert into \"like\" (user_id, post_id) values (?, ?)");
            preparedStatement.setInt(1, user.getId());
            preparedStatement.setInt(2, post.getId());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getByAuthor(int author_id) {

    }

    @Override
    public List<Like> getByPost(int id) {
        List<Like> likes = new ArrayList<>();
        try {
            PreparedStatement st = con.prepareStatement("select * from \"like\" where post_id = ?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                likes.add(new Like(
                        rs.getInt("user_id"),
                        rs.getInt("post_id")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return likes;

    }
}
