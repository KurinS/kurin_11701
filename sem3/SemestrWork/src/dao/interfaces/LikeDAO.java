package dao.interfaces;

import enities.Like;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface LikeDAO {
    void deleteLike(int id);
    void addLike(HttpServletRequest request);
    void getByAuthor(int author_id);
    List<Like> getByPost(int id);
}
