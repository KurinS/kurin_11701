package dao.interfaces;

import enities.User;

public interface UserDAO {
    User getByUsername(String username);
    User getById(int id);
    void addNewUser(String name, String surname, String username, String password, Boolean sex);
    void update(String name, String surname, String username, String oldusername,String password, Boolean sex);
}
