package dao.interfaces;

import enities.Post;
import enities.User;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface PostDAO {
    Post getByTitle(String title);
    Post getPostById(int id);
    void deletePost(Post post);
    List<Post> getEvenPosts();
    List<Post> getPosts();
    Post getLastPost();
    List<Post> getUnevenPosts();
    int save(HttpServletRequest request);
    void update(Post post, String text);
    List<Post> getPostsByAuthorId(int author_id);
    List<Post> getPostsByCategory(String category);
}
