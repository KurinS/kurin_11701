package dao.interfaces;

import enities.Dislike;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface DislikeDAO{
    void addDislike(HttpServletRequest request);
    void deleteDislike(int id);
    void getByAuthor(int author_id);
    List<Dislike> getByPost(int id);
}
