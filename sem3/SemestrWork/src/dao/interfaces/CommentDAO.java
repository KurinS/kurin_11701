package dao.interfaces;

import enities.Comment;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface CommentDAO {

    void save(HttpServletRequest request);
    void deleteComment(HttpServletRequest request);
    List<Comment> getByPostId(int id);
    Comment getById(int id);
    Comment getLastComment();
    List<Comment> getComments();

}
