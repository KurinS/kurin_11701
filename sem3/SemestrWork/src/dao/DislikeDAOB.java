package dao;

import dao.interfaces.DislikeDAO;
import dao.interfaces.PostDAO;
import dao.interfaces.UserDAO;
import enities.Dislike;
import enities.Like;
import enities.Post;
import enities.User;
import services.UserService;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DislikeDAOB implements DislikeDAO {
    Connection con = SingletonConnection.getConnection();
    private static UserDAO userDAO = new UserDAOB();
    private static PostDAO postDao = new PostDAOB();

    @Override
    public void addDislike(HttpServletRequest request) {
        User user;
        Post post;

        try {
            user = new UserDAOB().getByUsername(new UserService().getCurrentUser(request).getUsername());
            post = new PostDAOB().getPostById(Integer.parseInt(request.getPathInfo().substring(1)));
            PreparedStatement preparedStatement = con.prepareStatement(
                    "insert into dislike (user_id, post_id) values (?, ?)");
            preparedStatement.setInt(1, user.getId());
            preparedStatement.setInt(2, post.getId());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteDislike(int id) {

    }

    @Override
    public void getByAuthor(int author_id) {

    }

    @Override
    public List<Dislike> getByPost(int id) {
        List<Dislike> dislikes = new ArrayList<>();
        try {
            PreparedStatement st = con.prepareStatement("select * from dislike where post_id = ?");
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                dislikes.add(new Dislike(
                        rs.getInt("user_id"),
                        rs.getInt("post_id")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dislikes;

    }

}

