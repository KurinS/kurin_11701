package dao;

import dao.interfaces.UserDAO;
import enities.User;

import java.sql.*;

public class UserDAOB implements UserDAO {



    @Override
    public User getByUsername(String username) {
        Connection connection = SingletonConnection.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from \"user\" where username = ?");
            preparedStatement.setString(1, username);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                String usname = rs.getString("username");
                String uspass = rs.getString("password");
                String usna = rs.getString("name");
                String ussurname = rs.getString("surname");
                int usid = rs.getInt("id");
                boolean ussex = rs.getBoolean("sex");
                return new User(usid, usna, ussurname, usname, uspass, ussex);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User getById(int id) {
        Connection connection = SingletonConnection.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from \"user\" where id = ?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                String usname = rs.getString("username");
                String uspass = rs.getString("password");
                String usna = rs.getString("name");
                String ussurname = rs.getString("surname");
                int usid = rs.getInt("id");
                boolean ussex = rs.getBoolean("sex");
                return new User(usid, usna, ussurname, usname, uspass, ussex);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void addNewUser(String name, String surname, String username, String password, Boolean sex) {
        Connection connection = SingletonConnection.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "insert into \"user\" (name, surname, username, password, sex) values (?,?,?,?,?)");
            preparedStatement.setString(1, name );
            preparedStatement.setString(2, surname);
            preparedStatement.setString(3, username);
            preparedStatement.setString(4, password);
            preparedStatement.setBoolean(5,sex);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(String name, String surname, String username, String oldusername, String password, Boolean sex) {
        Connection connection = SingletonConnection.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "update \"user\" set name=?,surname=?,username=?, password=?, sex=? where username=?", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, name );
            preparedStatement.setString(2, surname);
            preparedStatement.setString(3, username);
            preparedStatement.setString(4, password);
            preparedStatement.setBoolean(5,sex);
            preparedStatement.setString(6, oldusername);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}




