package enities;

public class User {
    private int id;
    private String name;
    private String surname;
    private String username;
    private String password;
    private boolean sex;

    public User(int id, String name, String surname, String username, String password, boolean sex) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.sex = sex;
    }

    public String getSex(){
        if (isSex()){
            return " Female";
        } else {
            return "Male";
        }
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean isSex() {
        return sex;
    }
}
