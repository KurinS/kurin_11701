package enities;

public class Dislike {
    private int post_id;
    private int user_id;

    public int getPost_id() {
        return post_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public Dislike(int post_id, int user_id) {
        this.post_id = post_id;
        this.user_id = user_id;
    }
}
