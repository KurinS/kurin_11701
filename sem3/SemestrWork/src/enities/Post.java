package enities;

public class Post {
    private int id;
    private User author;
    private String category;
    private String text;
    private String title;

    public Post(int id, User author, String category, String text, String title) {
        this.id = id;
        this.author = author;
        this.category = category;
        this.text = text;
        this.title = title;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAuthor(User author) {
        this.author = author;
    }


    public void setCategory(String category) {
        this.category = category;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public int getId() {
        return id;
    }

    public User getAuthor() {
        return author;
    }

    public String getCategory() {
        return category;
    }

    public String getText() {
        return text;
    }

}
