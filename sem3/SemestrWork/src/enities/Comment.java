package enities;

public class Comment {
    private int id;
    private Post post_id;
    private User user_id;
    private String text;

    public Comment(int id, Post post_id, User user_id, String text) {
        this.id = id;
        this.post_id = post_id;
        this.user_id = user_id;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Post getPost_id() {
        return post_id;
    }

    public void setPost_id(Post post_id) {
        this.post_id = post_id;
    }

    public User getUser_id() {
        return user_id;
    }

    public void setUser_id(User user_id) {
        this.user_id = user_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
