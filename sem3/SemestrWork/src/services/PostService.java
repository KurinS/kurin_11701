package services;

import dao.PostDAOB;
import enities.Post;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PostService {
    PostDAOB postDAO = new PostDAOB();
    public int save(HttpServletRequest request) {
       return postDAO.save(request);
    }

    public void update(Post post, String text) {
        post.setText(text);
        postDAO.update(post, text);
    }

    public List<Post> getPosts(Post post){

      return postDAO.getPosts();
    }

}
