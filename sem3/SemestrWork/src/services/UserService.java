package services;

import dao.UserDAOB;
import enities.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class UserService {
    private UserDAOB userDAO = new UserDAOB();
    public User getCurrentUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("current_user");
        return user;
    }

    public User authenticate(HttpServletRequest request) {
        String username = request.getParameter("username");
        if (username != null) {
            User user = userDAO.getByUsername(username);
            if (user == null) {
                return null;
            }
            String password = request.getParameter("password");
            if (password.equals(user.getPassword())) {
                return user;
            } else {
                return null;
            }
        }
        return null;
    }

    public User register(HttpServletRequest request){
        String username = request.getParameter("username");
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String  password = request.getParameter("password");
        Boolean sex = Boolean.valueOf(request.getParameter("sex"));
        if (username != null) {
            if (uniqueLogin(username)) {
                userDAO.addNewUser(name, surname, username, password, sex);
                return userDAO.getByUsername(request.getParameter("username"));
            } else return null;
        }
        return null;
    }

    private boolean uniqueLogin(String username) {
        if(userDAO.getByUsername(username) == null) {
            return true;
        }else{
            return false;
        }
    }



    public void authorize(User current_user, HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("current_user", current_user);
    }
}

