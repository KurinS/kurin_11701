package servlets;

import dao.UserDAOB;
import enities.User;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import services.UserService;

import javax.swing.*;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.reflect.MalformedParameterizedTypeException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistrationServlet extends javax.servlet.http.HttpServlet {
    private UserService userService = new UserService();

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        if (userService.register(request) != null) {
            if (userService.getCurrentUser(request) != null) {
                request.getSession().setAttribute("username",request.getParameter("username"));
                response.sendRedirect("/profile/");
            } else {
                User current_user = userService.authenticate(request);
                if (current_user != null) {
                    userService.authorize(current_user, request);
                    request.getSession().setAttribute("username",request.getParameter("username"));
                    response.sendRedirect("/profile/");
                } else {
                    response.sendRedirect("/reg?err_mess=too_bad_login");
                }
            }
            } else {
                response.sendRedirect("/reg");

            }
        }


    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        response.setContentType("text/html");
        Configuration cfg = ConfigSingleton.getConfig(getServletContext());
        Template tmpl = cfg.getTemplate("registration.ftl");
        HashMap<String, Object> root = new HashMap<>();
        root.put("form_url", request.getRequestURI());
        try {
            tmpl.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
