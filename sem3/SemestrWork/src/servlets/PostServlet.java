package servlets;

import dao.CommentDAOB;
import dao.PostDAOB;
import enities.Comment;
import enities.Post;
import enities.User;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import services.PostService;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class PostServlet extends HttpServlet {
    CommentDAOB commentDAOB = new CommentDAOB();
    UserService userService = new UserService();
    PostDAOB postDAOB = new PostDAOB();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        String path = request.getPathInfo();
        Configuration cfg = ConfigSingleton.getConfig(getServletContext());
        Template tmpl = cfg.getTemplate("post.ftl");
        User user = userService.getCurrentUser(request);
        //System.out.println(path);
        List<Comment> comments = commentDAOB.getByPostId(Integer.parseInt(path.substring(1)));
        HashMap<String, Object> root = new HashMap<>();
        root.put("form_url", request.getRequestURI());
        root.put("user", user);
        root.put("comment", comments);
        try {
            tmpl.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
            System.out.println(path);
        }
    }
}
