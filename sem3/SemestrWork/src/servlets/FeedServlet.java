package servlets;

import dao.CommentDAOB;
import dao.DislikeDAOB;
import dao.LikeDAOB;
import dao.PostDAOB;
import enities.*;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import services.PostService;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class FeedServlet extends HttpServlet {
    private UserService userService = new UserService();
    private PostService postService = new PostService();
    private CommentDAOB commentDAOB = new CommentDAOB();
    private DislikeDAOB dislikeDAOB = new DislikeDAOB();
    private LikeDAOB likeDAOB = new LikeDAOB();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (userService.getCurrentUser(request) == null) {
            response.sendRedirect("/auth");
        } else if (request.getPathInfo() == null) {
            int id = postService.save(request);
            response.sendRedirect("/feed/" + id);
        }




        }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("like") != null){
            likeDAOB.addLike(request);
        }
        if(request.getParameter("dislike") != null){
            dislikeDAOB.addDislike(request);
        }
        if (request.getParameter("text") != null){
            commentDAOB.save(request);
        }
        if(request.getParameter("delete") != null) {
            commentDAOB.deleteComment(request);
        }
        response.setContentType("text/html");
            User user = userService.getCurrentUser(request);
            String path = request.getPathInfo();
            if (request.getPathInfo() == null) {
                Configuration cfg = ConfigSingleton.getConfig(getServletContext());
                Template tmpl = cfg.getTemplate("feed.ftl");
                List<Post> allposts = (new PostDAOB()).getPosts();
                List<Post> posts = (new PostDAOB()).getEvenPosts();
                List<Post> posts1 = (new PostDAOB()).getUnevenPosts();
                HashMap<String, Object> root = new HashMap<>();
                root.put("user", user);
                root.put("form_url", request.getRequestURI());
                root.put("posts1", posts1);
                root.put("posts", posts);
                root.put("aposts", allposts);

                try {
                    tmpl.process(root, response.getWriter());
                } catch (TemplateException e) {
                    e.printStackTrace();
                }
            } else  if (userService.getCurrentUser(request) == null) {
                response.sendRedirect("/auth");
            } else if (isNumber(path.substring(1))) {
                Configuration cfg = ConfigSingleton.getConfig(getServletContext());
                Template tmpl = cfg.getTemplate("post.ftl");
                List<Comment> comments = commentDAOB.getByPostId(Integer.parseInt(path.substring(1))) ;
                Post post = (new PostDAOB()).getPostById(Integer.parseInt(path.substring(1)));
                List<Dislike> dislikes = dislikeDAOB.getByPost(Integer.parseInt(request.getPathInfo().substring(1)));
                List<Like> likes = likeDAOB.getByPost(Integer.parseInt(request.getPathInfo().substring(1)));
                HashMap<String, Object> root = new HashMap<>();
                root.put("form_url", request.getRequestURI());
                root.put("post", post);
                root.put("comment", comments);
                root.put("count_comment", comments.size());
                root.put("user", user);
                root.put("likes", likes.size());
                root.put("dislikes", dislikes.size());


                try {
                    tmpl.process(root, response.getWriter());
                } catch (TemplateException e) {
                    e.printStackTrace();
                }
            }
        }


    private boolean isNumber(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
}
