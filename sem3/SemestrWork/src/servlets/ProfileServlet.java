package servlets;

import dao.UserDAOB;
import enities.User;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import services.PostService;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class ProfileServlet extends HttpServlet {
    private UserService userService = new UserService();
    private PostService postService = new PostService();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        if (request.getParameter("out") != null){
            System.out.println("hello out");
            request.getSession().setAttribute("current_user", null);
        }
        if (userService.getCurrentUser(request) == null) {
            response.sendRedirect("/auth");
        } else {
            response.setContentType("text/html");
            User user;
            if (request.getParameter("id") != null) {
                user = (new dao.UserDAOB()).getById(Integer.parseInt(request.getParameter("id")));
            } else {
                user = userService.getCurrentUser(request);

            }
            Configuration cfg = ConfigSingleton.getConfig(getServletContext());
            if (request.getPathInfo().equals("/")) {
                Template tmpl = cfg.getTemplate("profile.ftl");
                HashMap<String, Object> root = new HashMap<>();
                root.put("form_url", request.getRequestURI());
                root.put("name", user.getName());
                root.put("surname", user.getSurname());
                root.put("email", user.getUsername());
                root.put("sex", user.getSex());
                root.put("user", user);
                try {
                    tmpl.process(root, response.getWriter());
                } catch (TemplateException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
