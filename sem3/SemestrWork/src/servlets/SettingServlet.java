package servlets;

import dao.UserDAOB;
import enities.User;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class SettingServlet extends HttpServlet {
    private UserDAOB userDAOB = new UserDAOB();
    private UserService userService = new UserService();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       String name = request.getParameter("name");
       String surname = request.getParameter("surname");
       String username = request.getParameter("username");
       String oldusername = (String) request.getSession().getAttribute("username");
       String password = request.getParameter("password");
       Boolean sex = Boolean.valueOf(request.getParameter("sex"));
       userDAOB.update(name, surname, username, oldusername, password, sex);
       request.getSession().setAttribute("current_user",userDAOB.getByUsername(username));
       response.sendRedirect("/profile/");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        if (userService.getCurrentUser(request) == null) {
            response.sendRedirect("/auth");
        } else {
            response.setContentType("text/html");
            User user;
            if (request.getParameter("id") != null) {
                user = (new dao.UserDAOB()).getById(Integer.parseInt(request.getParameter("id")));
            } else {
                user = userService.getCurrentUser(request);

            }
            Configuration cfg = ConfigSingleton.getConfig(getServletContext());
            Template tmpl = cfg.getTemplate("settings.ftl");
            HashMap<String, Object> root = new HashMap<>();
            root.put("form_url", request.getRequestURI());
            root.put("name", user.getName());
            root.put("surname", user.getSurname());
            root.put("email", user.getUsername());
            root.put("password", user.getPassword());
            root.put("sex", user.getSex());
            root.put("user", user);
            try {
                tmpl.process(root, response.getWriter());
            } catch (TemplateException e) {
                e.printStackTrace();
            }
        }
    }
}
