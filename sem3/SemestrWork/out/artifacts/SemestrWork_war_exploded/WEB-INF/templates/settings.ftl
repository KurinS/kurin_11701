<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Settings</title>
    <link rel="stylesheet" href="../css/signUp.css">
    <link rel="stylesheet" href="../css/myNavbar.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
          integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
</head>
<body style="background-color: #F5F5F5">

<nav class="navbar sticky-top navbar-expand-lg color">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">HistoryProject</a>
        </div>
        <ul class="nav navbar-nav ">
            <li class="active "><a class="nav-link" href="/feed">FEED</a></li>
            <li><a class="nav-link" href="/topics">TOPICS</a></li>
            <li><a class="nav-link" href="/add">ADD POST</a></li>
            <li><a class="nav-link" href="/settings">SETTINGS</a></li>
            <li><a class="nav-link" href="/about">ABOUT US</a></li>
        </ul>
         <#if user??>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav-link" href="/profile/"></span>MY PROFILE</a></li>
        </ul>
         <#else>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav-link" href="/reg"><span class="fa fa-user-plus"></span> Sign Up</a></li>
            <li><a class="nav-link" href="/auth"><span class="fa fa-sign-in-alt"></span> Log in</a></li>
        </ul>
         </#if>
    </div>
</nav>

<div class="container">
    <div class="post">
        <form method="post">
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" class="form-control" placeholder="" id="name" value="${name}" >
            </div>
            <div class="form-group">
                <label>Surname</label>
                <input type="text" class="form-control" placeholder="old surname" id="surname" value="${surname}" name="surname">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" name="username" class="form-control" placeholder="old " id="username" value="${email}">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" placeholder="required field" value="${password}" id="password" name="password">
            </div>
            <div class="form-group">
                <label>Sex</label>
                <select class="custom-select" name="sex ">

                    <option selected value="${sex}">Choose...</option>
                    <option value="True">Male</option>
                    <option value="False">Female</option>
                </select>
            </div>
            <div class="form-group">
                <label>Change photo</label>
                <input type="file" class="form-control-file">
            </div>
            <div align="center">
                <button type="submit" class="butt">Edit</button>
            </div>
        </form>
    </div>
</div>


</body>
</html>