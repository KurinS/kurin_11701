<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>About us</title>

    <link rel="stylesheet" href="../css/signUp.css">
    <link rel="stylesheet" href="../css/myNavbar.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
          integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

</head>
<body style="background-color: #F5F5F5">

<nav class="navbar sticky-top navbar-expand-lg color">
    <div class="container ">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">HistoryProject</a>
        </div>
        <ul class="nav navbar-nav ">
            <li class="active "><a class="nav-link" href="/feed">FEED</a></li>
            <li><a class="nav-link" href="/topics">TOPICS</a></li>
            <li><a class="nav-link" href="/add">ADD POST</a></li>
            <li><a class="nav-link" href="/settings">SETTINGS</a></li>
            <li><a class="nav-link" href="/about">ABOUT US</a></li>
        </ul>
          <#if user??>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav-link" href="/profile/"></span>MY PROFILE</a></li>
        </ul>
          <#else>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav-link" href="/reg"><span class="fa fa-user-plus"></span> Sign Up</a></li>
            <li><a class="nav-link" href="/auth"><span class="fa fa-sign-in-alt"></span> Log in</a></li>
        </ul>
          </#if>
    </div>
</nav>

<h1 align="center" style="margin-top: 40px ">Our Team</h1>
<div class="container-fluid">
    <div class="row justify-content-center" style="padding: 40px">
        <div class="col">
            <div class="postPlace">
                <h3 align="center">Dimitrius</h3>
                <p align="center">Product owner</p>
                <img src="../../images/dima.jpg" class="picture">
            </div>
        </div>
        <div class="col">
            <div class="postPlace">
                <h3 align="center">Camillina</h3>
                <p align="center">Front-end developer</p>
                <img src="../../images/kamila.jpg" class="picture">
            </div>
        </div>
        <div class="col">
            <div class="postPlace">
                <h3 align="center">Stefano</h3>
                <p align="center">Back-end developer</p>
                <img src="../../images/stepa.jpg" class="picture">
            </div>
        </div>
    </div>
</div>

</body>
</html>