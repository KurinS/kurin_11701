<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Post</title>

    <link rel="stylesheet" href="../css/signUp.css">
    <link rel="stylesheet" href="../css/myNavbar.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
          integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
</head>
<body style="background-color: #F5F5F5">

<nav class="navbar sticky-top navbar-expand-lg color">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">HistoryProject</a>
        </div>
        <ul class="nav navbar-nav ">
            <li class="active "><a class="nav-link" href="/feed">FEED</a></li>
            <li><a class="nav-link" href="/topics">TOPICS</a></li>
            <li><a class="nav-link" href="/add">ADD POST</a></li>
            <li><a class="nav-link" href="/settings">SETTINGS</a></li>
            <li><a class="nav-link" href="/about">ABOUT US</a></li>
        </ul>
         <#if user??>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav-link" href="/profile/"></span>MY PROFILE</a></li>
        </ul>
         <#else>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav-link" href="/reg"><span class="fa fa-user-plus"></span> Sign Up</a></li>
            <li><a class="nav-link" href="/auth"><span class="fa fa-sign-in-alt"></span> Log in</a></li>
        </ul>
         </#if>
    </div>
</nav>

<div class="container">
    <div class="post">
        <form method="post">
            <div class="form-group">
                <label>Title</label>
                <input type="text" class="form-control" placeholder="required field" name="title">
            </div>
            <div class="form-group">
                <label>Text</label>
                <textarea class="form-control" rows="5" name="text" placeholder="required field"></textarea>
            </div>
            <div class="form-group">
                <label>Add photo material</label>
                <input type="file" class="form-control-file">
            </div>
            <div class="form-group">
                <label>Choose 1-3 categories</label>
                <select class="custom-select" name="category">
                    <option value="Ancient Greece">Ancient Greece</option>
                    <option value="Ancient Rome">Ancient Rome</option>
                    <option value="Ancient Egypt">Ancient Egypt</option>
                    <option value="Middle Ages">Middle Ages</option>
                    <option value="Dinozaur Era">Dinozaur Era</option>
                    <option value="Human Evolution">Human Evolution</option>
                    <option value="Ancient India">Ancient India</option>
                    <option value="Russian History">Russian History</option>
                    <option value="History of America">History of America</option>
                    <option value="Ottoman Empire">Ottoman Empire</option>
                    <option value="Wars">Wars</option>
                    <option value="Fra East">Fra East</option>
                </select>
            </div>
            <div class="form-check">
                <label class="form-check-label">

                    <input type="checkbox" name="checkanon" class="form-check-input" value="False">Anonymous post
                </label>
            </div>
            <div align="center">
                <button type="submit" class="butt">Publish</button>
            </div>
        </form>
    </div>
</div>

</body>
</html>