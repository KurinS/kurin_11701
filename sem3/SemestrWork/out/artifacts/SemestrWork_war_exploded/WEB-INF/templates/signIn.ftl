<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sign In</title>
    <meta charset="utf-8">

    <link rel="stylesheet" href="../css/signUp.css">
    <link rel="stylesheet" href="../css/myNavbar.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
          integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

</head>
<body style="background-color: #F5F5F5">

<nav class="navbar sticky-top navbar-expand-lg color">
<div class="container">
    <div class="navbar-header">
        <a class="navbar-brand" href="/">HistoryProject</a>
    </div>
    <ul class="nav navbar-nav ">
        <li class="active "><a class="nav-link" href="/feed">FEED</a></li>
        <li><a class="nav-link" href="/topics">TOPICS</a></li>
        <li><a class="nav-link" href="/add">ADD POST</a></li>
        <li><a class="nav-link" href="/settings">SETTINGS</a></li>
        <li><a class="nav-link" href="/about">ABOUT US</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a class="nav-link" href="/reg"><span class="fa fa-user-plus"></span> Sign Up</a></li>
        <li><a class="nav-link" href="/auth"><span class="fa fa-sign-in-alt"></span> Log in</a></li>
    </ul>
</div>
</nav>

<div class="container">
    <div class="wrapper" >
        <div class="form" style="margin-top: 100px">
            <div>
                <h1 align="center" style="font-family: font_nav">Sign In</h1>
            </div><br>
            <form method ='post'>
                <div class="form-group">
                    <input type="text" class="form-control" name="username" placeholder="Enter your username">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Enter your password">
                </div>
                <div class="form-group">
                    <input type="checkbox" name="remember"> Remember me<br>
                </div>
                <div align="center" class="form-group" >
                    <input type="submit" class="butt" value="Sign In">
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>