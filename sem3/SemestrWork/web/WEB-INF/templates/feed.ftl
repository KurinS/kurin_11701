<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>Feed</title>
    <link rel="stylesheet" href="../css/signUp.css">
    <link rel="stylesheet" href="../css/myNavbar.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
          integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

</head>
<body style="background-color: #F5F5F5">

<nav class="navbar sticky-top navbar-expand-lg color">
    <div class="container ">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">HistoryProject</a>
        </div>
        <ul class="nav navbar-nav ">
            <li class="active "><a class="nav-link" href="/feed">FEED</a></li>
            <li><a class="nav-link" href="/topics">TOPICS</a></li>
            <li><a class="nav-link" href="/add">ADD POST</a></li>
            <li><a class="nav-link" href="/settings">SETTINGS</a></li>
            <li><a class="nav-link" href="/about">ABOUT US</a></li>
        </ul>
         <#if user??>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav-link" href="/profile/"></span>MY PROFILE</a></li>
        </ul>
         <#else>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav-link" href="/reg"><span class="fa fa-user-plus"></span> Sign Up</a></li>
            <li><a class="nav-link" href="/auth"><span class="fa fa-sign-in-alt"></span> Log in</a></li>
        </ul>
         </#if>
    </div>
</nav>

<div class="container-fluid">
    <div class="row justify-content-center" style="padding: 40px">
        <div class="col" id="column1">
            <#list posts as p>
            <div class="postPlace">
                <h3 name="title">${p.getTitle()}</h3>
                <h5 name="category">${p.getCategory()}</h5>
                <p align="justify" name="text">${p.getText()}</p>
                <a href="feed/${p.getId()}">
                <button type="button" class="more"><b>LEARN MORE</b></button></a>
            </div>
            </#list>
            <#--<div class="postPlace">-->
                <#--<img src="../../../../../../Downloads/history_project_html/images/rome.jpg" alt="Italian Trulli" class="picture">-->
                <#--<h3>Title</h3>-->
                <#--<h5>Ancient Rome, Ancient Egypt</h5>-->
                <#--<p align="justify">Ancient Egypt was a civilization of ancient North Africa, concentrated along the lower reaches of the Nile River in the place that is now the country Egypt. Ancient Egyptian civilization followed prehistoric Egypt and coalesced around 3100 BC (according to conventional Egyptian chronology)[1] with the political unification of Upper and Lower Egypt under Menes (often identified with Narmer).</p>-->
                <#--<button type="button" class="more"><b>LEARN MORE</b></button>-->
            <#--</div>-->

        </div>
        <div class="col" id="column2">
           <#list posts1 as p>
               <div class="postPlace">
                   <h3 name="title">${p.getTitle()}</h3>
                   <h5 name="category">${p.getCategory()}</h5>
                   <p align="justify" name="text">${p.getText()}</p>
                   <a href="feed/${p.getId()}">
                       <button type="button" class="more"><b>LEARN MORE</b></button></a>
               </div>
           </#list>
        </div>
    </div>
</div>
</body>
</html>