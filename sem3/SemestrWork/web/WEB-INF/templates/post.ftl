<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Post</title>

    <link rel="stylesheet" href="../css/signUp.css">
    <link rel="stylesheet" href="../css/myNavbar.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
          integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

</head>
<body style="background-color: #F5F5F5">

<nav class="navbar sticky-top navbar-expand-lg color">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">HistoryProject</a>
        </div>
        <ul class="nav navbar-nav ">
            <li class="active "><a class="nav-link" href="/feed">FEED</a></li>
            <li><a class="nav-link" href="/topics">TOPICS</a></li>
            <li><a class="nav-link" href="/add">ADD POST</a></li>
            <li><a class="nav-link" href="/settings">SETTINGS</a></li>
            <li><a class="nav-link" href="/about">ABOUT US</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav-link" href="/profile/"></span>MY PROFILE</a></li>
        </ul>

    </div>
</nav>

<div class="container">
    <div class="post">
        <h1 id="title" name="title">${post.getTitle()}</h1>
        <i><h5 id="categories" name="category">${post.getCategory()}</h5></i>
        <div style="text-align: right">
            <h5 id="author" name="author">by ${post.getAuthor().getName()}</h5>
        </div>
        <div id="photos">
            <img src="../../../../../../Downloads/history_project_html/images/egypt.jpg" alt="Italian Trulli" class="picture">
            <img src="../../../../../../Downloads/history_project_html/images/egypt2.jpg" alt="Italian Trulli" class="picture">
        </div>
        <div>
            <p id="textOfPost" align="justify"> ${post.getText()}
            </p>
        </div>
        <div style="text-align: right">
            <div>
                <form method="get">
                <button id="like" class="icon" name="like" type="submit"><span class="fa fa-thumbs-up fa-2x"></span></button>
                <h2 id="amountOfLikes" class="like">${likes}</h2>
                </form>

                <form method="get">
                <button id="dislike" name="dislike" class="icon" type="submit"><span class="fa fa-thumbs-down fa-2x"></span></button>
                <h2 id="amountOfDislikes" class="like">${dislikes}</h2>
                </form>

                <button id="comment" class="icon" type="button"><span class="fa fa-comments fa-2x"></span></button>
                <h2 id="amountOfComments" class="like">${count_comment}</h2>
            </div>
        </div>
    </div>
</div>
<form method="get">
<div class="container">
        <div class="form-group">
            <textarea class="form-control" rows="5" id="text" name="text"></textarea>
        </div>

        <div class="form-check">
            <label class="form-check-label">
                <input type="checkbox" class="form-check-input" value="True">Anonymous comment
            </label>
        </div>
        <div align="right">
            <button type="submit" name="text"  class="btn btn-secondary">Add comment</button>
        </div>
</div>
</form>
<div class="container">
    <h2>Comments</h2>
</div>
<div class="container">
    <#list comment as c>
    <div class="comment">
        <div style="text-align: right">
            <form method="get">
                <input type="hidden" name="delete" value="${c.getId()}">
            <button id="delete" name="delete" class="icon" type="submit"><span
                    class="fa fa-times fa-2x"></span></button>
            </form>
        </div>
        <div style="margin: 20px">
            <p id="textOfComment" align="justify">
                ${c.getText()}
            </p>
        </div>
        <div style="text-align: right">
            <h6 id="user">by ${c.getUser_id().getName()} </h6>
        </div>
    </div>
    </#list>
</div>
</body>
</html>