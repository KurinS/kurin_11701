<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hello</title>

    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/myNavbar.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
          integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

</head>
<body>

<nav class="navbar sticky-top navbar-trans navbar-expand-lg">
    <div class="container ">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">HistoryProject</a>
        </div>
        <ul class="nav navbar-nav ">
            <li class="active "><a class="nav-link" href="/feed">FEED</a></li>
            <li><a class="nav-link" href="/topics">TOPICS</a></li>
            <li><a class="nav-link" href="/add">ADD POST</a></li>
            <li><a class="nav-link" href="/settings">SETTINGS</a></li>
            <li><a class="nav-link" href="/about">ABOUT US</a></li>
        </ul>
         <#if user??>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav-link" href="/profile/"></span>MY PROFILE</a></li>
        </ul>
         <#else>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav-link" href="/reg"><span class="fa fa-user-plus"></span> Sign Up</a></li>
            <li><a class="nav-link" href="/auth"><span class="fa fa-sign-in-alt"></span> Log in</a></li>
        </ul>
         </#if>
    </div>
</nav>

<div class="container">
    <h1 class="hello">We are not makers of history. <br>We are made of history.</h1>
</div>

</body>
</html>