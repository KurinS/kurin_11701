<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile</title>
    <link rel="stylesheet" href="../css/signUp.css">
    <link rel="stylesheet" href="../css/myNavbar.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
          integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body style="background-color: #F5F5F5">
<nav class="navbar sticky-top navbar-expand-lg color">
    <div class="container ">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">HistoryProject</a>
        </div>
        <ul class="nav navbar-nav ">
            <li class="active "><a class="nav-link" href="/feed">FEED</a></li>
            <li><a class="nav-link" href="/topics">TOPICS</a></li>
            <li><a class="nav-link" href="/add">ADD POST</a></li>
            <li><a class="nav-link" href="/settings">SETTINGS</a></li>
            <li><a class="nav-link" href="/about">ABOUT US</a></li>
        </ul>
         <#if user??>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav-link" href="/profile/"></span>MY PROFILE</a></li>
        </ul>
         <#else>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav-link" href="/reg"><span class="fa fa-user-plus"></span> Sign Up</a></li>
            <li><a class="nav-link" href="/auth"><span class="fa fa-sign-in-alt"></span> Log in</a></li>
        </ul>
         </#if>
    </div>
</nav>

<div class="container">
    <div class="post">
        <div class="d-flex justify-content-between">
            <div class="p-2 marg">
                <div class="card" style="width: 400px">
                    <img class="card-img-top photo" src="/../../images/avatar.jpg" alt="Card image cap">
                    <div class="card-body">
                        <div class="spoiler disabled">
                            <h3 class="descr-button" style="text-align: center">Personal Information <span
                                    class="fa fa-arrow-down"></span></h3>
                            <div class="descr-text">
                                <div class="part"><h4 style="display: inline">Name: </h4>
                                    <p id="username" style="display: inline">${name}</p></div>
                                <div class="part"><h4 style="display: inline">Surname: </h4>
                                    <p id="surname" style="display: inline">${surname}</p></div>
                                <div class="part"><h4 style="display: inline">Email: </h4>
                                    <p id="email" style="display: inline">${email}</p></div>
                                <div class="part"><h4 style="display: inline">Sex: </h4>
                                    <p id="sex" style="display: inline">${sex}</p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="p-2 marg">
                <div style="text-align: center">
                    <div>
                        <a href="/add">
                    <button class="button"><span class="fa fa-pencil-alt"></span> ADD POST</button></a>
                    </div>
                    <div>
                        <a href="/my_posts">
                        <button class="button" type="submit" ><span class="fa fa-list-ul"></span> SEE ALL MY POSTS</button>
                        </a>
                    </div>
                    <div>
                        <a href="/settings">
                        <button class="button"><span class="fa fa-cogs"></span> SETTINGS</button>
                            </a>
                    </div>
                    <div>
                        <form method="get">
                            <a href="/about">
                            <input type="hidden" value="out" name="out">
                        <button class="button" type="submit"><span class="fa fa-sign-out-alt"></span> LOG OUT</button>
                            </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('.question-text').hide(300);
        $(document).on('click', '.descr-button', function (e) {
            e.preventDefault()
            $(this).parents('.spoiler').toggleClass("active").find('.descr-text').slideToggle();
        });
    });
</script>

</body>
</html>