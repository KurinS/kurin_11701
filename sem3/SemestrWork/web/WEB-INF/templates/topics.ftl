<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Topics</title>

    <link rel="stylesheet" href="../css/topics.css">
    <link rel="stylesheet" href="../css/myNavbar.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
          integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

</head>
<body>
<nav class="navbar sticky-top navbar-expand-lg color">
    <div class="container ">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">HistoryProject</a>
        </div>
        <ul class="nav navbar-nav ">
            <li class="active "><a class="nav-link" href="/feed">FEED</a></li>
            <li><a class="nav-link" href="/topics">TOPICS</a></li>
            <li><a class="nav-link" href="/add">ADD POST</a></li>
            <li><a class="nav-link" href="/settings">SETTINGS</a></li>
            <li><a class="nav-link" href="/about">ABOUT US</a></li>
        </ul>
         <#if user??>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav-link" href="/profile/"></span>MY PROFILE</a></li>
        </ul>
         <#else>
        <ul class="nav navbar-nav navbar-right">
            <li><a class="nav-link" href="/reg"><span class="fa fa-user-plus"></span> Sign Up</a></li>
            <li><a class="nav-link" href="/auth"><span class="fa fa-sign-in-alt"></span> Log in</a></li>
        </ul>
         </#if>
    </div>
</nav>

<h3 class="choose" align="center">
    <span>CHOOSE AT LEAST ONE TOPIC: </span>
</h3>
<div style="margin-bottom: 30px">
    <table style="margin: auto">
        <tr>
            <td>
                <div align="center">
                    <form method="get">
                    <input name="topic" value="Middle Ages" type="hidden">
                    <button type="submit" name="topic" class="topic"><span class="fa fa-church fa-10x"></span>
                        <h5 class="topic">MIDDLE AGES</h5></button>
                    </form>
                </div>
            </td>
            <td>
                <div align="center">
                    <form method="get">
                    <input name="topic" type="hidden" value="Ancient Greece">
                    <button type="submit" class="topic"><span class="fa fa-landmark fa-10x"></span>
                        <h5 class="topic">ANCIENT GREECE</h5></button>
                    </form>
                </div>
            </td>
            <td>
                <div align="center">
                    <form method="get">
                    <input name="topic" type="hidden" value="Ancient Egypt">
                    <button type="submit" class="topic"><span class="fa fa-ankh fa-10x"></span>
                        <h5 class="topic">ANCIENT EGYPT</h5></button>
                    </form>
                </div>
            </td>
            <td>
                <div align="center">
                    <form method="get">
                    <input name="topic" type="hidden" value="Ancient Rome">
                    <button type="submit" class="topic" value="Ancient Rome"><span class="fa fa-thumbs-up fa-10x"></span>
                        <h5 class="topic">ANCIENT ROME</h5></button>
                    </form>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div align="center">
                    <form method="get">
                    <input name="topic" type="hidden" value="Dinozaur Era">
                    <button type="submit" class="topic"><span class="fa fa-dragon fa-10x"></span>
                        <h5 class="topic">DINOZAUR ERA</h5></button>
                    </form>
                </div>
            </td>
            <td>
                <div align="center">
                    <form method="get">
                    <input name="topic" type="hidden" value="Human Evolution">
                    <button type="submit" class="topic"><span class="fa fa-fire fa-10x"></span>
                        <h5 class="topic">HUMAN EVOLUTION</h5></button>
                    </form>
                </div>
            </td>
            <td>
                <div align="center">
                    <form method="get">
                    <input name="topic" type="hidden" value="Ancient India">
                    <button type="submit" class="topic"><span class="fa fa-gopuram fa-10x"></span>
                        <h5 class="topic">ANCIENT INDIA</h5></button>
                    </form>
                </div>
            </td>
            <td>
                <div align="center">
                    <form method="get">
                    <input name="topic" type="hidden" value="Russian History">
                    <button type="submit" class="topic"><span class="fa fa-flag fa-10x"></span>
                        <h5 class="topic">RUSSIAN HISTORY</h5></button>
                    </form>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div align="center">
                    <form method="get">
                    <input name="topic" type="hidden" value="History of America">
                    <button type="submit" class="topic"><span class="fa fa-globe-americas fa-10x"></span>
                        <h5 class="topic">HISTORY OF AMERICA</h5></button>
                    </form>
                </div>
            </td>
            <td>
                <div align="center">
                    <form method="get">
                    <input name="topic" type="hidden" value="Ottoman Empire">
                    <button type="submit" class="topic"><span class="fa fa-mosque fa-10x"></span>
                        <h5 class="topic">OTTOMAN EMPIRE</h5></button>
                    </form>
                </div>
            </td>
            <td>
                <div align="center">
                    <form method="get">
                    <input name="topic" type="hidden" value="Wars">
                    <button type="submit" class="topic"><span class="fa fa-bomb fa-10x"></span>
                        <h5 class="topic">WARS</h5></button>
                    </form>
                </div>
            </td>
            <td>
                <div align="center">
                    <form method="get">
                    <input name="topic" type="hidden" value="Far East">
                    <button type="submit" class="topic"><span class="fa fa-torii-gate fa-10x"></span>
                        <h5 class="topic">FAR EAST</h5></button>
                     </form>
                </div>
            </td>
        </tr>

    </table>
</div>
</body>
</html>