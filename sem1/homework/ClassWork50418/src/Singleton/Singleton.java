package Singleton;

public class Singleton {

    private static Earth earth =  null;

    public static Earth getEarth() {
        if(earth == null){
            earth = new Earth();
        }
        return earth;
    }


}
