import java.util.*;

public class MyTree {

    private Node root;

    public MyTree(int n) {
        root = new Node();
        createTree(root, n); //повесить на root дерево с n вершинами
    }

    private void createTree(Node node, int n) {
        node.setValue(n);
        if (n > 1) {
            int nl = n / 2;
            int nr = n - 1 - nl;
            if (nl > 0) {
                node.setLeft(new Node());
                createTree(node.getLeft(), nl);
            }
            if (nr > 0) {
                node.setRight(new Node());
                createTree(node.getRight(), nr);

            }


        }
    }

    public static void printTree(Node node, int h) {
        if (node != null) {
            printTree(node.getRight(), h + 1);
            for (int i = 0; i < h; i++) {
                System.out.println("  ");
            }
            System.out.println(node.getValue());
            printTree(node.getLeft(), h + 1);
        }
    }

    public void print() {
        printTree(root, 0);
    }


    static void depthFirsts(Node node) {
        if (node.getLeft() != null) {
            depthFirsts(node.getLeft());
        }

        if (node.getRight() != null) {
            depthFirsts(node.getRight()
            );
        }

    }


    static void depthSecond(Node node) {
        if (node.getRight() != null) {
            depthSecond(node.getRight());
        }

        if (node.getLeft() != null) {
            depthSecond(node.getLeft());
        }

    }

    static void lengthFisrt(Node node){
        if((node.getLeft() != null) || node.getRight() != null){
            lengthFisrt(node.getLeft());
            lengthFisrt(node.getRight());
        }
    }


    public void queueOrd(Node node){
        Queue <Node> q = new LinkedList<>();
        q.offer(node);
        while( !q.isEmpty()){
            Node p = q.poll();
            if(p.getLeft() != null){
                q.offer(p.getLeft());
            }


            if(p.getRight()  != null){
                q.offer(p.getRight());
            }
        }



    }

    public void stackOrd(Node node){
        Stack<Node> stack = new Stack<>();

        stack.push(node);


    }




    public static void main(String[] args) {
        MyTree t = new MyTree(15);
        t.print();
    }
}
