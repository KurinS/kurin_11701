import CollectionSingletons.ContinentSingleton;
import entities.Continent;

import java.util.List;

public class Task1 {
    public static void main(String[] args) {

        List<Continent> continents = ContinentSingleton.getContinentCollection();


        Continent continent = continents.stream().max((c1, c2) -> c1.countCountries() > c2.countCountries() ? 1 : -1).get();

        for (Continent continent1: continents){
            if (continent1.countCountries()> continent.countCountries()){
                continent = continent1;
            }
        }
        System.out.println(continent.getName());
    }
}

