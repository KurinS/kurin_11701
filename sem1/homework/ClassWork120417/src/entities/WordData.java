package entities;

public class WordData {
    private String word;
    private int count;

    public WordData(String word, int count) {
        this.word = word;
        this.count = count;
    }

    public String getWord() {
        return word;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setWord(String word) {
        this.word = word;
    }

}

//ToDo:
//Classes - Continent Country CarMaker
//коллекции на базе этих классов
//0 Task - вывести слово с максимальным использоваием
//1 Task - вывести континент с макс количеством стран
//2 Task - вывести Makers из Европы