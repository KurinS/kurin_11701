package entities;


public class CountryData{
    public int id;
    private String name;
    private Continent continent;


    public CountryData(int id, String name, Continent continent) {
        this.id = id;
        this.name = name;
        this.continent = continent;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CountryData(String attr, int i) { }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Continent getContinent() {
        return continent;
    }

    public void setContinent(Continent continent) {
        this.continent = continent;
    }
}

