package entities;

import CollectionSingletons.CountrySingleton;

import java.util.List;

public class Continent {
    private int id;
    private String name;
    private List<CountryData> countries;




    public Continent(int id, String name, List<CountryData> countries) {
        this.id = id;
        this.name = name;
        this.countries = countries;
    }

    public Continent(int i, String arg) {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CountryData> getCountries() {
        return countries;
    }

    public void setCountries(List<CountryData> countries) {
        this.countries = countries;
    }

    public int countCountries() {
        return countries.size();
    }
}