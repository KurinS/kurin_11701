package entities;

public class CarMaker {
    int id;
    String maker;
    String fullName;
    String Country;

    public CarMaker(int id, String maker, String fullName, String country) {
        this.id = id;
        this.maker = maker;
        this.fullName = fullName;
        Country = country;

    }

    public  CarMaker(int id, String attr, String attr1, CountryData country) {

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }
}
