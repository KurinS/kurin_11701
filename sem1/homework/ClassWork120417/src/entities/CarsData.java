package entities;

public class CarsData{
    int id;
    int mpg;
    int cylindres;
    int Edispl;
    int weight;
    double accelerate;
    int year;

    public CarsData(int id, int mpg, int cylindres, int edispl, int weight, double accelerate, int year) {
        this.id = id;
        this.mpg = mpg;
        this.cylindres = cylindres;
        Edispl = edispl;
        this.weight = weight;
        this.accelerate = accelerate;
        this.year = year;
    }




    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMpg() {
        return mpg;
    }

    public CarsData(String attr, int i) { }

    public void setMpg(int mpg) {
        this.mpg = mpg;
    }

    public int getCylindres() {
        return cylindres;
    }

    public void setCylindres(int cylindres) {
        this.cylindres = cylindres;
    }

    public int getEdispl() {
        return Edispl;
    }

    public void setEdispl(int edispl) {
        Edispl = edispl;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getAccelerate() {
        return accelerate;
    }

    public void setAccelerate(double accelerate) {
        this.accelerate = accelerate;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
