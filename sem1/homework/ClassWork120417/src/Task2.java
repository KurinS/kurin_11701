import CollectionSingletons.CarMakerSingleton;
import CollectionSingletons.ContinentSingleton;
import com.sun.corba.se.spi.ior.IdentifiableContainerBase;
import entities.CarMaker;
import entities.Continent;

import java.util.List;

public class Task2 {
    final static int EuId = 2;

    public static void main(String[] args) {
       List<CarMaker>carMakers = CarMakerSingleton.getCarMakerCollection();
        for (CarMaker carmaker1 :
                carMakers) {
            if(carmaker1.getCountry().equals("2"))
            System.out.println(carmaker1.getFullName());
        }



    }
}
