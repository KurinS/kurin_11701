package CollectionSingletons;

import entities.CountryData;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CountrySingleton{
    public static List<CountryData> countryDataCollection;
    private static String filename = "resources/countries";

    public static List<CountryData> getCountryDataCollection(){

        if(countryDataCollection == null){
            countryDataCollection = new ArrayList<>();

            BufferedReader br = null;
            try {
                br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            String  line = null;
            try {
                line = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            while(line != null){
                String[] attrs = line.split(",");

                countryDataCollection.add(new CountryData(attrs[0], Integer.parseInt(attrs[1])));
                try {
                    line = br.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return countryDataCollection;
    }

}
