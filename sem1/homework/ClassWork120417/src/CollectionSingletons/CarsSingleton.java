package CollectionSingletons;

import entities.CarsData;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CarsSingleton {

    public static List<CarsData> carsDataCollection;
    private static String filename = "resosurces/cars-data.csv";

    public static List<CarsData> getCarsDataCollection() throws IOException {

        if (carsDataCollection == null){
            carsDataCollection = new ArrayList<>();

            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            String  line =  br.readLine();
            while(line != null){
                String[] attrs = line.split(",");
                carsDataCollection.add(new CarsData(attrs[0], Integer.parseInt(attrs[1])));
                line = br.readLine();
            }
        }

        return carsDataCollection;
    }

}
