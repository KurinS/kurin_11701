package CollectionSingletons;

import CollectionSingletons.CountrySingleton;
import entities.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class CarMakerSingleton {
    private static List<CarMaker> carMakerCollection;
    private static List<CountryData> counc = CountrySingleton.getCountryDataCollection();
    private static String filename = "src//resources//car-makers.csv";
    public static List<CarMaker> getCarMakerCollection(){
        if (carMakerCollection == null){
            carMakerCollection = new ArrayList<>();
            try {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(
                                new FileInputStream(filename)));
                String line = br.readLine();

                int value4;
                while (line != null){
                    String [] attrs = line.split(",");
                    for (CountryData country: counc) {
                        if (!attrs[0].equals("Id")) {
                            if (attrs[3].equals("null")){
                                value4 = 0;
                            } else{
                                value4 = Integer.parseInt(attrs[3]);
                            }
                            if (value4 == country.getId()) {
                                carMakerCollection.add(new CarMaker());
                            }
                        }
                    }
                    line = br.readLine();
                }

            } catch (IOException e){
                e.printStackTrace();
            }
        }
        return carMakerCollection;
    }
}
