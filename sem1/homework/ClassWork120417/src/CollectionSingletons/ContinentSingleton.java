package CollectionSingletons;

import entities.Continent;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ContinentSingleton {
    private static List<Continent> continentCollection;
    private static String filename = "resources/continents.csv";

    public static List<Continent> getContinentCollection(){
        if (continentCollection == null){
            continentCollection = new ArrayList<>();
            try{
                BufferedReader br = null;
                try {
                    br = new BufferedReader(
                            new InputStreamReader(
                                    new FileInputStream(filename)
                            )
                    );
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                String line = br.readLine();
                line = br.readLine();
                while (line!=null){
                    String [] args = line.split(",'|'");
                    continentCollection.add(new Continent(Integer.parseInt(args[0]),args[1]));
                    line = br.readLine();
                }

            } catch (FileNotFoundException e){
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
            }
        }
        return continentCollection;
    }

}
