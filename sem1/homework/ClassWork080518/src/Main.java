import java.net.MalformedURLException;


public class Main{

    public static void main(String[] args) throws MalformedURLException {
        String url = "https://ru.wikipedia.org/wiki/%D0%9B%D0%BE%D0%BD%D0%B4%D0%BE%D0%BD";
        String extension = "pdf";
        (new Downloader(url, extension)).start();

    }

}