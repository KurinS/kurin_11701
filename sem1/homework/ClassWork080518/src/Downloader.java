import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Downloader{
    private String url;
    private String extension;
    private final static String RE_TEMPLATE_FOR_URL = "";
    private final String reForUrl;

    public String  buildReForUrl(){
        String re = "href=\"http://[^\"]+" + "[.]"+ extension + "\"";
        return re;
    }

    public Downloader(String url, String extension) {
        this.url = url;
        this.extension = extension;
        this.reForUrl = buildReForUrl();
    }
    public void start(){
        Pattern p = Pattern.compile(this.reForUrl);
        //open URL
        try {
            URL urlObj = new URL(this.url);
            LineNumberReader reader = new LineNumberReader(new InputStreamReader(urlObj.openStream()));
            //read lines for IS
            String line = reader.readLine();
            while (line != null){
                //for every line look for matches
                Matcher m = p.matcher(line);
                while (m.find()){
                    String fileUrl = m.group();
                    System.out.println(fileUrl);
                    fileUrl = fileUrl.replace("href=\"", "");
                    //for every match download file
                    (new FileDownloader(fileUrl.replace("\"", ""))).download();
                }
                line = reader.readLine();
            }
        } catch (MalformedURLException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }

    }
}
