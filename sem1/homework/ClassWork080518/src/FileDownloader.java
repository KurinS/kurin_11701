import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.Semaphore;

public class FileDownloader implements Runnable{

    private static Semaphore semaphore = new Semaphore(3, true);
    private String fileUrl;
    private Thread t;

    public FileDownloader(String fileUrl) {
        this.fileUrl = fileUrl;
        t = new Thread(this);
    }


    public void download() throws IOException{
        t.start();
    }

    @Override
    public void run() {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        InputStream is = null;
        OutputStream os = null;
        String SAVE_DIR = "C:\\";
        try {
            System.out.println(fileUrl);
            URL url = new URL(fileUrl);
            URLConnection conn = url.openConnection();
            conn.addRequestProperty("User-Agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
            is = (new URL(fileUrl).openStream());
            String fileName = new File(url.getPath()).getName();
            File parent = new File(SAVE_DIR);
            if (parent.isDirectory() || parent.mkdirs()) {
                String s = SAVE_DIR + File.separatorChar + fileName;
                os = new FileOutputStream(s);
                byte[] buffer = new byte[1024];
                int length = is.read(buffer);
                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                    length = is.read(buffer);
                }
            } else {
                throw new IOException("Cannot create " + SAVE_DIR);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }



        semaphore.release();
    }
}
