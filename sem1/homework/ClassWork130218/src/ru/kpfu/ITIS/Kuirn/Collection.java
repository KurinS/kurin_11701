package ru.kpfu.ITIS.Kuirn;
public interface Collection <Integer> {
    int add();
    boolean contains(Object o);
    int remove();
    int size();
    int clear();
    boolean isEmpty();
}
