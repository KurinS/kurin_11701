package ru.kpfu.ITIS.Kuirn;

public class LinkedCollection implements Collection<Integer> {

    private class Elem{
        Integer  value;
        Elem next;
    }

    private  Elem head;
    private int n = 0;



    @Override
    public int add() {

        return 0;
    }

    @Override
    public boolean contains(Object o) {
        if(o instanceof Integer){
            Integer i = (Integer) o;
            Elem p = head;
            while(p != null){

                if(p.value == i){
                    return true;
                }
                p = p.next;
            }
            return false;
        } else{
            return false;
        }
    }



    @Override
    public int remove() {

        return 0;
    }

    @Override
    public int size() {

        return 0;
    }

    @Override
    public int clear() {

        return 0;
    }

    @Override
    public boolean isEmpty() {

        return n==0;
    }
}
