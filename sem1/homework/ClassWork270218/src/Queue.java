import java.util.LinkedList;

public class Queue<E>{
    LinkedList<E> qu;

    public Queue() {
        qu = new LinkedList<>();
    }

    public void push(E value){
        qu.push(value);
    }

    public E pop(){
        return qu.removeFirst();
    }

    public boolean isEmpty(){
        return qu.size() == 0;
    }
}