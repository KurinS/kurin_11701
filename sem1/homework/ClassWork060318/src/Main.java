import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Main{


    public static void main(String[] args) throws FileNotFoundException {
        HashMap<Character,Integer> mp = new HashMap<>();
        Scanner sc  = new Scanner(new File("file"));
        while(sc.hasNext()){
            Character c = sc.next().charAt(0);
            if (mp.containsKey(c)){
                int count = mp.get(c);
                count++;
                mp.put(c,count);
            } else{
                mp.put(c,1);
            }
        }

        Set<HashMap.Entry<Character,Integer>>set = mp.entrySet();
        System.out.println(Arrays.toString(set.toArray()));
    }
}
