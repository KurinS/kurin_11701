package Task2;

import sun.awt.windows.ThemeReader;

import java.io.*;

public class MainTask2 extends Thread{

    private File source;
    public static File copyedfile = new File("res/copyOfFile23");


    @Override
    public void run(){
        FileInputStream fileinstream = null;
        try {
            fileinstream = new FileInputStream(this.source);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            FileOutputStream fileoutstream = new FileOutputStream(copyedfile);
            try {
                byte[] buffer = new byte[4096];
                int length;
                while ((length = fileinstream.read(buffer)) > 0) {
                    fileoutstream.write(buffer, 0, length);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    fileoutstream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                fileinstream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public MainTask2(File source, File copyedfile) {
        this.source = source;
        this.copyedfile = copyedfile;

    }

    public static void main(String[] args) {

        Thread t1 = new MainTask2(new File("res/file2"), new File("res/copyOfFile23"));
        Thread t2 = new MainTask2(new File("res/file3"), new File("res/copyOfFile23"));
        t1.start();
        t2.start();


    }
}