import java.io.*;

public class MainTask1 {

    public static File filename = new File("res/file1");
    public static File copyfile = new File("res/copyOfFile1");



    public static void main(String[] args) throws FileNotFoundException {
        FileInputStream fileinstream = new FileInputStream(filename);

        try {
            FileOutputStream fileoutstream = new FileOutputStream(copyfile);
            try {
                byte[] buffer = new byte[4096];
                int length;
                while ((length = fileinstream.read(buffer)) > 0) {
                    fileoutstream.write(buffer, 0, length);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    fileoutstream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                fileinstream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}