public class Task0 {

    private Elem head;


    private void push(int value){
        Elem p = new Elem(value, head);
        head = p;
    }

    public int pop(){
        int value = head.getValue();
        head = head.getNext();
        return value;
    }

    public boolean isEmpty(){
        return true;
    }
}
