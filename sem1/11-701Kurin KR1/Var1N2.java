import java.util.Scanner;


public class Var1N2 {
	public static void main(String[] args) {
		
		int count = 0;

		Scanner sc =  new Scanner(System.in);

		int n = sc.nextInt();

		int [] a = new int[n];

		for (int i = 0; i < n; i++){
			a[i] = sc.nextInt();

		} 

		double funct = 0;
		int factr = 1;
		int test = 0;

		for (int i = 0; i < n; i++){
			if (a[i] % 2 == 0) {
				test = 1;
			}
		}

		if (test == 1){

		for(int j = 1; j < n; j++){
			factr = j * (j - 1);

				for (int i = 1; i < n; i++ ){
					int factrarg = a[i]*a[i-1];
					funct = Math.pow(a[i],2) + (Math.pow(2,i) + factr) * a[i] - factrarg;
					if (funct == 0 ){
						count= count + a[i];

					} 
				}
			
		}
		
		System.out.println(count);
		}else{
			System.out.println("error");
		}		
	}
}