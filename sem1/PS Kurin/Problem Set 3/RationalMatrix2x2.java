/**
 *author Stepan Kurin
 *11-701
 *Problem Set 3 Task07PS3
 */
public class RationalMatrix2x2 {
    public static void main(String[] args) {
        RationalFraction a1 = new RationalFraction(2, 3);
        RationalFraction a2 = new RationalFraction(5, 3);
        RationalFraction a3 = new RationalFraction(7, 13);
        RationalFraction a4 = new RationalFraction(7, 3);
        RationalMatrix2x2 mat1 = new RationalMatrix2x2(a3, a4, a1, a2);
        RationalMatrix2x2 mat2 = new RationalMatrix2x2(a4, a1, a2, a3);
        RationalMatrix2x2 mat0 = new RationalMatrix2x2();
        RationalMatrix2x2 mat4;
        boolean x, y;
        System.out.println(mat1.toString());
        System.out.println(mat2.toString());
        System.out.println(mat0.toString());

        System.out.println("det mat1 " + mat1.det());
        System.out.println("mult vect \n" + mat1.multVector(new RationalVector2D(a1, a4)));

        mat0 = mat1.add(mat2);
        System.out.println("add \n" + mat0.toString());
        mat0 = mat1.mult(mat2);
        System.out.println("mult \n" + mat0.toString());


    }

    private RationalFraction[][] a = new RationalFraction[3][3];

    public void assignment(RationalFraction[][] x) {
        this.a[1][1] = x[1][1];
        this.a[1][2] = x[1][2];
        this.a[2][1] = x[2][1];
        this.a[2][2] = x[2][2];
    }

    public RationalFraction[][] assignmentX(RationalFraction x, RationalFraction y, RationalFraction z, RationalFraction u) {
        a[1][1] = x;
        a[1][2] = y;
        a[2][1] = z;
        a[2][2] = u;
        return a;
    }

    RationalMatrix2x2() {
        RationalFraction[][] x = new RationalFraction[3][3];
        x = assignmentX(new RationalFraction(), new RationalFraction(), new RationalFraction(), new RationalFraction());
        assignment(x);
    }

    RationalMatrix2x2(RationalFraction a) {
        RationalFraction[][] x = new RationalFraction[3][3];
        x = assignmentX(a, a, a, a);
        assignment(x);
    }

    RationalMatrix2x2(RationalFraction x, RationalFraction y, RationalFraction z, RationalFraction u) {
        RationalFraction[][] r = new RationalFraction[3][3];
        r = assignmentX(x, y, z, u);
        assignment(r);
    }

    public RationalMatrix2x2 add(RationalMatrix2x2 mat) {
        RationalMatrix2x2 mat2 = new RationalMatrix2x2(this.a[1][1].add(mat.a[1][1]), this.a[1][2].add(mat.a[1][2]), this.a[2][1].add(mat.a[2][1]), this.a[2][2].add(mat.a[2][2]));
        return mat2;
    }


    public RationalMatrix2x2 mult(RationalMatrix2x2 mat) {
        RationalFraction a1, a2, a3, a4;
        a1 = this.a[1][1].mult(mat.a[1][1]).add(this.a[1][2].mult(mat.a[2][1]));
        a2 = this.a[1][2].mult(mat.a[1][2]).add(this.a[1][2].mult(mat.a[2][2]));
        a3 = this.a[2][1].mult(mat.a[1][1]).add(this.a[2][2].mult(mat.a[2][1]));
        a4 = this.a[2][2].mult(mat.a[1][2]).add(this.a[2][2].mult(mat.a[2][2]));
        RationalMatrix2x2 mat2 = new RationalMatrix2x2(a1, a2, a3, a4);
        return mat2;
    }

    public double det() {
        return (this.a[1][1].add(this.a[2][2])).sub(this.a[2][1].mult(this.a[1][2])).value();
    }


    public RationalVector2D multVector(RationalVector2D vect) {
        RationalVector2D vect2 = new RationalVector2D(this.a[1][1].mult(vect.getX()).add(this.a[1][2].mult(vect.getY())), this.a[2][1].mult(vect.getX()).add(this.a[2][2].mult(vect.getY())));
        return vect2;
    }

    public String toString() {
        return a[1][1] + " " + a[1][2] + "\n" + a[2][1] + " " + a[2][2] + "\n" + "___________";
    }
}
