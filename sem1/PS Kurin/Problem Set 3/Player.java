/**
 *author Stepan Kurin
 *11-701
 * for problem Set 3 Task01PS3
 */
public class Player {

   private String name;
   private int hp;
   public static final int MAXPOWER = 9;

    public Player(int hp) {
        this.hp = hp;
    }



    public Player(String name) {
        this.name = name;
    }


    public Player(String name,int hp) {
        this.name = name;
        this.hp = hp;

    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void hit(Player p2, int hitpower) {
        double p = (MAXPOWER - hitpower) / (double)(MAXPOWER);
        if(Math.random() <= p){
            p2.setHp(p2.getHp() - hitpower) ;
        }

    }

}
