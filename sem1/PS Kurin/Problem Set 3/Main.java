import java.util.Scanner;

/**
 * @author Stepan Kurin
 * 11-701
 * Problem Set 3 Task 01PS3
 */

public class Main {
   public static Scanner sc;

   public static void main(String [] args){
        System.out.println("Enter your name");
        Player p1 = new Player(sc.next(), 10);
        System.out.println("Enter your name");
        Player p2 = new Player(sc.next(), 10);

        boolean turn = false;
        while (true) {

            if (turn) {
            	System.out.println(" ");
                System.out.println(p1.getName() + " your turn");
                System.out.println(p1.getHp() + " your hp");
                int hitpower = inputPower();
                p1.hit(p2, hitpower);
            } else {
            	System.out.println(" ");
                System.out.println(p2.getName() + " your turn");
                System.out.println(p2.getHp() + " your hp");
                int hitpower = inputPower();
                p2.hit(p1, hitpower);
            }
            if (p1.getHp() <= 0) {
            	System.out.println(" ");
                System.out.println(p1.getName() + "you lose");
                return;

            }

            if (p2.getHp() <= 0) {
            	System.out.println(" ");
                System.out.println(p2.getName() + " you lose");
                return;
            }

            turn = !turn;

        }

    }

    private static int inputPower() {
        while(true){
            int hitpower = sc.nextInt();
            if(hitpower >= 0 && hitpower <= 9){
                return hitpower;
            }else {
                System.out.println("Please, input correc");
            }
        }
    }
}