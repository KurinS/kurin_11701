import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
*author Stepan Kurin
*11-701
*Problem Set 2 Task03PS2
*/

public class Task03PS2 {
    public static void main(String[] args) {

        Pattern pat = Pattern.compile("1+|0+|((10)+1?)|((01)+0?)");
        Matcher mat= pat.matcher("1010");
        System.out.println(mat.matches());
    }
}
