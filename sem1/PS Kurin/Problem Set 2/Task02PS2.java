/**
*@autrhor Stepan Kurin
*11-701
*Problem Set 2 Task02PS2
*/

import java.util.regex.Matcher;
import java.util.regex.Pattern;




public class Task02PS2 {
    public static void main(String[] args) {

        Pattern pat = Pattern.compile("([-+]?[1-9][0-9]+((\\.|\\,)[0-9]*(\\([0-9]+[1-9]\\))?)?)|0|[+-]0((\\.|\\,)[0-9]*(\\([0-9]+[1-9]\\))?)");
        Matcher mat= pat.matcher("-0.6");
        System.out.println(mat.matches());
    }
}

