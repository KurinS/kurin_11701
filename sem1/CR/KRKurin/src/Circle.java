public class Circle implements Measurable {

   private double radius;


    Circle(int radius){
        this.radius = radius;
    }

    public double getP() {
        return  2 * Math.PI * radius;
    }

    public double getS() {
        return Math.PI * radius * radius;
    }

}
