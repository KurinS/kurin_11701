/**
 @author Stepan Kurin
  *11-701
  *Problem Set 1 Task 01
 */
import  java.util.Scanner;
public class Task08PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = sc.nextDouble();
        double EPS = 1e-9;
        int k = 1;
        double chisl = Math.pow(-1, k) * Math.pow(x, 4*k + 1); ;
        double fact = 1;
        double znam = fact * (4 * k + 1);;
        double sum = chisl / znam;
        while (sum > EPS){
            fact = fact * 2 * k;
            chisl = Math.pow(-1, k) * Math.pow(x, 4*k + 1);
            znam = fact * (4 * k + 1);
            sum = sum + (chisl / znam);
            k++;
        }

        System.out.println(sum);
    }
}
