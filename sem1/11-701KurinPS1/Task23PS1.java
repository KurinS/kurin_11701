/**
 @author Stepan Kurin
  *11-701
  *Problem Set 1 Task 01
 */
import java.util.Scanner;
import java.util.Random;

public class Task23PS1 {
	public static void main(String[] args) {
		Scanner sc  = new Scanner(System.in);

		int r1  = sc.nextInt();
		int c1 = sc.nextInt();
		int r2 = sc.nextInt();
		int c2 = sc.nextInt();

		int [][] a  = new int [r1][c1];
		int [][] b = new int [r2][c2];
		int [][] c = new int [r1][c2];

		fill(a);
		fill(b);

		int sum = 0;

		if (r1 == c2){

			for (int i = 0; i < r1; i++){
				for (int j = 0; j < c2; j++){
					for ( int k = 0; k < r1; k++){
						sum += a[i][k] * b[k][j];
        			}
        		c[i][j] = sum;
				}
			}
		}
		
		if (r2 == c1){
			for (int i = 0; i < c1; i++){
				for (int j = 0; j < r2; j++){
					for ( int k = 0; k < r1; k++){
						sum += a[i][k] * b[k][j];
        			}
        		c[i][j] = sum;
				}
			}
		}
		for ( int i = 0; i < c.length; i++){
			for (int j = 0; j < c.length; j++){
				System.out.print(a[i][j] + " ");
				
			}
		
		System.out.println(" ");
		
		}
	}

	public static void fill(int [][] arr){
		Random r = new Random();
		for(int i = 0; i < arr.length; i++){
			for(int j = 0; j < arr.length; j++){
				arr[i][j] = r.nextInt(9)+1;
			}
		}
	}
}
