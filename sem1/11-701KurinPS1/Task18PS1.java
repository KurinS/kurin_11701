/**
 @author Stepan Kurin
  *11-701
  *Problem Set 1 Task 01
 */
import java.util.Scanner;

public class Task18PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int arr [] = new int [n];
        int arr2 []  = new int [n];
        for (int i = 0; i < n; i++){
            arr[i] = sc.nextInt();
        }
        int k = sc.nextInt();
        while ( k < n){
            arr2[n - k] = arr[k];
        }
        for (int i = 0; i < k; i++){
            arr2[i] = arr[n - i];
        }
        for (int i = 0; i < n; i++){
            System.out.println(arr2[i]);
        }
    }

}
