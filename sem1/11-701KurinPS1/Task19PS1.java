/**
 @author Stepan Kurin
  *11-701
  *Problem Set 1 Task 01
 */
import java.util.Random;
import java.util.Scanner;

public class Task19PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[][] a = new int[2*n+1][2*n+1];
        fill(a);
        show(a);
        System.out.println();
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                if (i < a.length/2 && j > i && j < a.length - i - 1){
                    a[i][j] = 0;
                }
                if (i > a.length/2 && j < i && j > a.length - i - 1){
                    a[i][j] = 0;
                }
            }
        }
        show(a);
    }
    
    public static void fill(int[][] a){
        Random r = new Random();
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                a[i][j] = r.nextInt(9)+1;
            }
        }
    }

    public static void show(int[][] a){
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
    }
}