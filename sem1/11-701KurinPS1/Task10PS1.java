import java.util.Scanner;

/**
 @author Stepan Kurin
  *11-701
  *Problem Set 1 Task 01
 */


public class Task10PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int [] arr = new int[n];
        boolean k = false;
        for(int i = 0; i < n; i++){
            arr[i] = sc.nextInt();
        }
        for(int i = 1; i < (n - 1) ; i++){
            if((arr[i] > arr[i - 1]) & (arr[i] > arr [i + 1]) & (i % 2 == 0)){
                k = true;

            }
        }
        System.out.println(k);

    }
}
