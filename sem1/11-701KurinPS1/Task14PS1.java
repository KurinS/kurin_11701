/**
 @author Stepan Kurin
  *11-701
  *Problem Set 1 Task 01
 */
import java.util.Scanner;
public class Task14PS1 {
    public static void main(String[] args) {
        Scanner sc  = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        int count = 0;
        int i = 0;
        while (n != 0){
            count  = (int) ( count + Math.pow(k, i) * (n % 10) );
            n = (int) n / 10;
            i++;
        }
        System.out.println(count);
    }
}
