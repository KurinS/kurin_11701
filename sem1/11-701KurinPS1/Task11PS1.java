/**
 @author Stepan Kurin
  *11-701
  *Problem Set 1 Task 01
 */
import java.util.Scanner;
public class Task11PS1 {
    public static void main(String[] args) {
        Scanner sc  = new Scanner(System.in);
        int n = sc.nextInt();
        int count = 0 ;
        int arr [] = new int[n];
        for(int i = 0; i < n; i++){
            arr[i] = sc.nextInt();
        }
        for(int i = 1; i < (n - 1) ; i++){
            if((arr[i] > arr[i - 1]) & (arr[i] > arr [i + 1]) & (i % 2 == 0)){
                count++;

            }
        }
        if(count == 2){
            System.out.println("true");
        }else{
            System.out.println("false");
        }
    }
}
