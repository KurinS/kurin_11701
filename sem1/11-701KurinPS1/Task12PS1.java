/**
@author Stepan Kurin
*11-701
*Problem Set 1 Task 01
*/

import java.util.Scanner;


public class Task12PS1{
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int m = sc.nextInt();
		int newnumnber = 0;
		System.out.println("");

		if (m % 2 == 0){
			while (m != 0){
				if (m % 2 == 0)
				System.out.print(m % 10);
				m = m / 10; 
			}
		}else{
			while (m != 0){
				if (m % 2 != 0)
				System.out.print(m % 10);
				m = m / 10;

			}
		}
	}
}