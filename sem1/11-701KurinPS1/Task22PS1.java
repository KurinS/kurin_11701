import java.util.Scanner;

/**
 @author Stepan Kurin
  *11-701
  *Problem Set 1 Task 22
 */
public class Task22PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int sum = 0;
        int sum1 = 0;
        int k = 0;
        int max = 0;
        int arr[][] = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = sc.nextInt();
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((i == j + k) & (sum > max)) {
                    sum = sum + arr[i][j];
                    max = sum;
                }
                k++;

            }
        }
        System.out.println(max);
    }
}