/**
 @author Stepan Kurin
  *11-701
  *Problem Set 1 Task 01
 */
import java.util.Scanner;

public class Task17PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k;
        int [] arr = new int[n];
        for (int i = 0; i < n; i++){
            arr[i] = sc.nextInt();
        }
        for (int i = 0; i < n; i++){
        k = arr[i];
        arr[i] = arr[n - i - 1];
        arr[n - i - 1] = k;
        }
        for(int i = 0; i < n; i++){
            System.out.println(arr[i]);
        }
    }
}
