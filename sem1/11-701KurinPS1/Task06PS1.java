/**
 @author Stepan Kurin
  *11-701
  *Problem Set 1 Task 01
 */
import java.util.Scanner;

public class Task06PS1{

    public static void main (String [] args){

        Scanner sc = new Scanner (System.in);
        int n = sc.nextInt();

        for (int i = 0; i <= 2 * n; i++){
            for (int j = 0; j <= 2 * n; j++){
                if(((i - n) * (i - n) + (j - n) * (j - n)) <= n * n){
                    System.out.print("0");
                }
                else{
                    System.out.print(" ");
                }
            }
            System.out.println(" ");
        }
    }
}
