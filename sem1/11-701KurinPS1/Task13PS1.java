/**
 @author Stepan Kurin
  *11-701
  *Problem Set 1 Task 01
 */
import java.util.Scanner;
public class Task13PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int count =(int) ((((Math.pow(10, n + 1) - 10) / 9) - n) / 9);

        System.out.println(count);


    }
}
