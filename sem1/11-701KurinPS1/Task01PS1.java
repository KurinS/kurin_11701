/**
 @author Stepan Kurin
  *11-701
  *Problem Set 1 Task 01
 */
import java.util.Scanner;

public class Task01PS1{
	
	public static void main (String [] args){
		
		Scanner sc = new Scanner (System.in);
		int n = sc.nextInt();
		int i,j,k;
		for ( i=0; i<n; i++){
			
			for (j = 0; j < (2 * n - i); j++){
				System.out.print(" ");
			}
			for (k = 0; k < (2 * i + 1); k++){
				System.out.print("*");
			}
			System.out.println("");
		}
		
		for (i = 0; i < n; i++){
			
			for (j = 0; j < (n - i); j++){
				System.out.print(" ");
			}
			for (k = 0; k < (2 * i + 1); k++){
				System.out.print("*");
			}
			for (j = 0; j < (2 * (n - i) - 1); j++){
				System.out.print(" ");
			}
			for (k = 0; k < (2 * i + 1); k++){
				System.out.print("*");
			}
			System.out.println ("");
		}
		
	}
	
}
