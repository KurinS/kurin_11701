/**
 @author Stepan Kurin
  *11-701
  *Problem Set 1 Task 01
 */
import java.util.Scanner;

public class Task16SP1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int count = 0;


        for (int i = 0; i < n; i++){
            int num = sc.nextInt();
            int chisl1 = 0;
            boolean flag = true;
            int chisl = num % 10;
            num = num / 10;
            while (num != 0){
                chisl1 = num % 10;
                if (chisl1 > chisl){
                    flag = true;
                    chisl = chisl1;
                    chisl1 = num % 10;

                }else {
                    flag = false;
                    break;
                }

            }
            if (flag = true){
                    count++;
            }
        }
    }
}
