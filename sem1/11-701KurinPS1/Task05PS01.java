/**
 @author Stepan Kurin
  *11-701
  *Problem Set 1 Task 05
 */
import java.util.Scanner;

public class Task05PS01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        double sum = 0;
        int factm = 1;
        int fact = 1;
        for (int m = 2; m < n; m++){
            factm = (m - 1) * factm;
            fact = 2 * m * fact;
            sum = sum +(double) (factm / fact);
        }

        System.out.println(sum);

    }
}
