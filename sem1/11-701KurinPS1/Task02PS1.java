/**
@author Stepan Kurin
*11-701
*Problem Set 1 Task 01
*/

import java.util.Scanner;


public class Task02PS1{
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int k = sc.nextInt();
		int m = sc.nextInt();

		int count = 0;

		if (k < m) {
			for (int i = k;i <= m; i++){
				if (i % 3 == 0){
					count++;
				}
			}
		}

		if (m < k) {
			for (int i = m;i <= k; i++){
				if (i % 3 == 0){
					count++;
				}
			}
		}

		System.out.println(count);
	}

}