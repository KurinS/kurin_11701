/**
 @author Stepan Kurin
  *11-701
  *Problem Set 1 Task 01
 */
import java.util.Scanner;

public class Task03PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        double EPS = 1e-6;
        double ch = 1;
        while (ch * ch < x){
            ch = ch +EPS;
        }
        System.out.println(ch);
    }
}
