import java.util.Scanner;
/**
 @author Stepan Kurin
  *11-701
  *Problem Set 1 Task 04
 */

public class Task04PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int i = 2;
        while (n != 1) {
            while ((n % i) == 0) {
                n = n / i;
                System.out.println(i);
            }
            i++;
        }
    }
}
 

