/**
 @author Stepan Kurin
  *11-701
  *Problem Set 1 Task 01
 */
import java.util.Scanner;

public class Task24PS1 {
    public static boolean check(int[][][] a) {
        // n - глубина
        int n = a.length;
        // m - строки
        int m = a[0].length;

        // сперва глубина
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (!rowIsDivisibleBy3(a[i][j]))
                    return false;
            }
        }
        // сперва уровни
        for (int j = 0; j < m; j++) {
            for (int i = 0; i < n; i++) {
                if (!rowIsDivisibleBy3(a[i][j]))
                    return false;
            }
        }
        return true;
    }

    public static boolean rowIsDivisibleBy3(int[] a) {
        for (int i : a) {
            if (i % 3 != 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n1 = sc.nextInt();
        int n2 = sc.nextInt();
        int n3 = sc.nextInt();
        int[][][] a = new int[n1][n2][n3];
        for (int i = 0; i < n1; i++) {
            for (int j = 0; j < n2; j++) {
                for (int k = 0; k < n3; k++) {
                    a[i][j][k] = sc.nextInt();
                }
            }
        }
        System.out.println(check(a));
    }
}