package Tasks;

import Singletons.AppelationsSingleton;
import Singletons.WinesSingleton;
import data.Appellations;
import data.Wine;

import java.io.IOException;
import java.util.List;

public class Task1 {

    public static void main(String[] args) throws IOException {
        System.out.println(getMaxAppellation());

    }

    private static String getMaxAppellation() throws IOException {
        List<Wine> wines = WinesSingleton.getWinesList();


        Wine wine = wines.stream().max((c1, c2) -> c1.countAppell() > c2.countAppell() ? 1 : -1).get();

        for (Wine wine1 : wines) {
            if (wine1.countAppell() > wine.countAppell()) {
                wine = wine1;
            }
        }

        return wine.getAppelation();
    }
}
