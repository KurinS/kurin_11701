package Singletons;
import data.Appellations;
import data.Grapes;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class AppelationsSingleton {
    private static List<Appellations> appellationsList;
    private static String filename = "src/resources/appellations.csv";

    public static List<Appellations> getAppellationsList() throws IOException {
        if(appellationsList == null){
            appellationsList = new ArrayList<>();
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
        String line =  br.readLine();
        while(line != null){
            String [] attrs = line.split(",");
            appellationsList.add((new Appellations(Integer.parseInt(attrs[0]), attrs[1], attrs[2], attrs[3],attrs[4])));
        }

        return appellationsList;
    }
}

