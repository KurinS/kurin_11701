package Singletons;

import data.Appellations;
import data.Wine;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class WinesSingleton {

        private static List<Wine> wineList;
        private static String filename = "src/resources/appellations.csv";

        public static List<Wine> getWinesList() throws IOException {
            if(wineList == null){
                wineList = new ArrayList<>();
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            String line =  br.readLine();
            while(line != null){
                String [] attrs = line.split(",");
                wineList.add((new Wine(Integer.parseInt(attrs[0]), attrs[1],attrs[2], attrs[3])));
            }

            return wineList;
        }
    }


