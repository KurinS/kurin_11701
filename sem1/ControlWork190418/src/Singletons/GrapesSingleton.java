package Singletons;


import data.Grapes;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class GrapesSingleton {

    public static List<Grapes> grapesList;
    private static String filename = "src/resources/grapes.csv";


    public static List<Grapes> getGrapesList() throws IOException {

        if(grapesList == null){
            grapesList = new ArrayList<>();
        }

            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            String line =  br.readLine();
            while(line != null){
                String [] attrs = line.split(",");
                grapesList.add((new Grapes(Integer.parseInt(attrs[0]),attrs[1],attrs[2])));
            }
        return grapesList;
    }
}
