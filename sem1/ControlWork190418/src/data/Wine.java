package data;

import java.util.List;

//No,Grape,Winery,Appelation,State,Name,Year,Price,Score,Cases,Drink
public class Wine{
    int number;
    String Grape;
    String Winery;
    String Appelation;
    List<Appellations> appellations;

    public List<Appellations> getAppellations() {
        return appellations;
    }

    public void setAppellations(List<Appellations> appellations) {
        this.appellations = appellations;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getGrape() {
        return Grape;
    }

    public void setGrape(String grape) {
        Grape = grape;
    }

    public String getWinery() {
        return Winery;
    }

    public void setWinery(String winery) {
        Winery = winery;
    }

    public String getAppelation() {
        return Appelation;
    }

    public void setAppelation(String appelation) {
        Appelation = appelation;
    }

    public Wine(int number, String grape, String winery, String appelation) {
        this.number = number;
        Grape = grape;
        Winery = winery;
        Appelation = appelation;
    }

    public int countAppell(){
        return(Integer.parseInt(String.valueOf(appellations.size())));
    }


}