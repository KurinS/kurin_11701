package data;

import java.util.List;

public class Appellations {

    int number;
    String appellation;
    String country;
    String state;
    String area;

    public Appellations(int number, String appellation, String country, String state, String area) {
        this.number = number;
        this.appellation = appellation;
        this.country = country;
        this.state = state;
        this.area = area;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getAppellation() {
        return appellation;
    }

    public void setAppellation(String appellation) {
        this.appellation = appellation;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }



}