package data;

public class Grapes {

    int id;
    String grape;
    String color;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGrape() {
        return grape;
    }

    public void setGrape(String grape) {
        this.grape = grape;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Grapes(int id, String grape, String color) {

        this.id = id;
        this.grape = grape;
        this.color = color;
    }
}
