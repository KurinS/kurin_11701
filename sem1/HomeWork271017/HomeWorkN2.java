import java.util.regex.*;


public class HomeWorkN2{

	public static void main(String[] args) {
		
		Pattern p = Pattern.compile("-?(0|[1-9][0-9]*)\\.[0-9]*\\([0-9]\\)");

		for (String n : args){
			Matcher m = p.matcher(n);
			System.out.println(m.matches());
		}
	}
	

}