import re
import multiprocessing as mp
from pip._vendor import requests


class FileDownloader:
    global pattern
    pattern = r'href="(?P<url>[a-zA-Z0-9_:/.]*\.'

    def __init__(self, url, extension):
        self.url = url
        self.pattern = pattern + extension + ')"'
        mp.freeze_support()
        self.semaphore = mp.Semaphore(3)

    def download_file(self, file_url):
        print("medved")
        self.semaphore.acquire()
        r = requests.get(file_url)
        filename = file_url.split('/')[-1]
        f = open('Downloads/' + filename, 'wb')
        f.write(r.content)
        f.close()
        self.semaphore.release()

    def download_files(self):
        print("preved")
        p = re.compile(self.pattern)
        r = requests.get(self.url)
        for line in r.iter_lines():
            for m in p.finditer(str(line)):
                mp.Process(target=FileDownloader.download_file,
                           args=(self, m.group('url'),)).start()


def main():
    fd = FileDownloader('https://ru.wikipedia.org/wiki/Сообщество_Христа', 'pdf')
    fd.download_files()


if __name__ == '__main__':
    main()
