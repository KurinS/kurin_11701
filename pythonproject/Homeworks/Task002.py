import math


class Vector2D:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __str__(self):
        return "<%s, %s>" % (self.x, self.y)

    def __add__(self, v):
        return Vector2D(self.x + v.x, self.y + v.y)

    def __mul__(self, n):
        if type(n) is Vector2D:
            return self.x * n.x + self.y * n.y
        else:
            return Vector2D(self.x * n, self.y * n)

    def __sub__(self, v):
        return Vector2D(self.x - v.x, self.y - v.y)

    def __len__(self):
        return math.sqrt(pow(self.x, 2) + pow(self.y, 2))


v1 = Vector2D(-4, 3)
v2 = Vector2D(3, 5)
v3 = Vector2D(3, -1)
v4 = Vector2D(-2, 7)
print(v3 * v4)
print(v1 * 2)
print(v1 + v2)
print(v1.__len__())
