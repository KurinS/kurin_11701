import re


def count(st):
    di = dict()
    reg = re.compile('[^a-zA-z]')
    st = reg.sub('', st).upper()
    for sym in st:
        if sym not in di:
            di[sym] = 1
        else:
            di[sym] += 1
    return di


print(count('Aa bba.'))
