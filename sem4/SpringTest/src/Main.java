import databases.Database;
import enities.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import views.View;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ApplicationContext ac =
                new ClassPathXmlApplicationContext("spring-config.xml");
        App app = ac.getBean(App.class);
        app.showUsers();
        app.showUserById(1);

    }
}
