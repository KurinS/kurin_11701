package views;

import enities.User;

import java.util.List;

public class JSONView implements View {
    @Override
    public String getUser(User user) {
        return "{\"name\": " + user.getName() + "}";
    }

    @Override
    public String showUsers(List<User> users) {
        return null;
    }

    @Override
    public String showUserFromDB(int id) {
        return null;
    }


}
