package views;

import enities.User;

import java.util.List;

public interface View {
    String getUser(User user);
    String showUsers(List<User> users);
    String showUserFromDB(int id);
    //String showUserFromDB(List<User> users)
}
