package views;

import databases.PostgresDB;
import enities.User;

import java.util.List;

public class HTMLView implements View{
    PostgresDB db = new PostgresDB();

    @Override
    public String getUser(User user) {
        return "<h1>" + user.getName() + user.getYear() + "</h1>";
    }

    @Override
    public String showUsers(List<User> users) {
        String result =  "<table>";
        for (User user :
                users) {
            result += "<tr><td>" + user.getName() + "</td>" +
                        "<td>" + user.getYear() + "</td></tr>";
        }
        return result + "</table>";
    }

    @Override
    public String showUserFromDB(int id) {
        return "<h2>" + db.getUserById(id).getName() + "<h2>";
    }
}
