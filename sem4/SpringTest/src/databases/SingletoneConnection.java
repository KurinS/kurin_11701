package databases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SingletoneConnection {
    private static Connection connection;

    public static Connection getConnection() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(
                        "jdbc:postgresql://localhost:5432/spring_test",
                        "postgres",
                        "1234");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }
}
