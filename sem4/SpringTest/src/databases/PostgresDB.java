package databases;

import enities.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class PostgresDB implements Database {
    Connection connection = SingletoneConnection.getConnection();

    @Override
    public User getUserById(int id) {
        try {
            PreparedStatement ps = connection.prepareStatement("select * from users where id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int user_id = rs.getInt("id");
                String user_name = rs.getString("name");
                int user_year = rs.getInt("year");
                String user_city = rs.getString("city");
                return new User(user_id, user_name, user_year, user_city);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<User> getUsers() {
        List<User> list = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement("select * from users");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int user_id = rs.getInt("id");
                String user_name = rs.getString("name");
                int user_year = rs.getInt("year");
                String user_city = rs.getString("city");
                list.add(new User(user_id, user_name, user_year, user_city));
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}

