package databases;

import enities.User;

import java.util.List;

public interface Database {
    User getUserById(int id);
    List<User> getUsers();
}
