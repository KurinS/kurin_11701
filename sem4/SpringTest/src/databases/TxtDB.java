package databases;

import enities.User;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TxtDB implements Database {
    Scanner sc;

    {
        try {
            sc = new Scanner(new File("C:/Users/Kurin/IdeaProjects/SpringTest/src/databases/text_db.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User getUserById(int id) {
        while (sc.hasNext()) {
            String[] line = sc.nextLine().split(" ");
            if (line[0].equals(id)) {
                int user_id = Integer.parseInt(line[0]);
                String user_name = line[1];
                int user_age = Integer.parseInt(line[2]);
                String user_city = line[3];
                return new User(user_id, user_name, user_age, user_city);
            }
        }
        return null;
    }

    @Override
    public List<User> getUsers() {
        List<User> users = new ArrayList<>();
        while (sc.hasNext()){
            String [] line = sc.nextLine().split(" ");
            int user_id = Integer.parseInt(line[0]);
            String user_name  = line[1];
            int user_age = Integer.parseInt(line[2]);
            String user_city = line[3];
            users.add(new User(user_id, user_name, user_age, user_city));
        }
        return users;
    }
}
