import databases.Database;
import views.View;

public class App {
    Database db;
    View view;

    public App(Database db, View view) {
        this.db = db;
        this.view = view;
    }

    public void showUsers(){
        System.out.println(view.showUsers(db.getUsers()));
    }
    public void showUserById(int  i){
        System.out.println(view.showUserFromDB(i));
    }
}

