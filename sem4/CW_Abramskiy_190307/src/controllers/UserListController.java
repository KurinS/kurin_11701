package controllers;

import DAO.TxtDB;
import models.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/part")
public class UserListController {
    TxtDB db = new TxtDB();
    @RequestMapping(method = RequestMethod.GET)
    public String getUsers(ModelMap mp){
        List<User> users = db.getUsers();
        mp.addAttribute("users", users);
        return "par";
    }

}
