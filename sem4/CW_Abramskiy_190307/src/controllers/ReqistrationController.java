package controllers;

import DAO.TxtDB;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;

@Controller
@RequestMapping("/reg")
public class ReqistrationController {
    TxtDB db = new TxtDB();
    @RequestMapping(method = RequestMethod.GET)
    public String reg(){
        return "registration";
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView auth(@RequestParam("id") int id, @RequestParam("name") String name, @RequestParam("age") int age,
                       @RequestParam("city") String city){
        db.insertUser(id, name, age, city);
        ModelAndView modelAndView = new ModelAndView("redirect:/part");
        return modelAndView;
    }

}
