package DAO;

import models.User;

import java.util.List;

public interface DBInterface {
    User getUserById(int id);
    List<User> getUsers();
    void insertUser(int id, String name, int age, String city);
}
