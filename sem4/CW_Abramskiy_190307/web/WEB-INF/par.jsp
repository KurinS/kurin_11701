<%--
  Created by IntelliJ IDEA.
  User: Kurin
  Date: 07/03/2019
  Time: 10:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
  <%--@elvariable id="users" type="java.util.List"--%>
  <c:forEach items="${users}" var="user">
    <ul>
        <li>${user.getId()}</li>
        <li>${user.getName()}</li>
        <li>${user.getAge()}</li>
        <li>${user.getCity()}</li>
    </ul>
  </c:forEach>
    <
</body>
</html>
