import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        X x = new X(2, 2, 3);
        Field[] fields = x.getClass().getDeclaredFields();
        Method[] methods = x.getClass().getDeclaredMethods();
        String table = "<table>";
        try {
            FileWriter wr = new FileWriter("src/out_fields.html");
            for (Method method :
                    methods) {
                if (method.getName().startsWith("get")) {
                    for (Field field :
                            fields) {
                        if (method.getName().substring(3).toLowerCase().equals(field.getName())) {
                            table += "<tr>" + "<td>" + method.getName().substring(3).toLowerCase() + "</td>" + "<td> value " + field.get(x) + "</td>" + "</tr>";
                        }
                    }
                }
            }
            wr.write(table + "</table>");
            wr.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }
}